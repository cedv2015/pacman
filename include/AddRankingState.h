#ifndef AddRankingState_H
#define AddRankingState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "GameState.h"
#include "MenuState.h"
#include "RankingState.h"
#include "PlayState.h"
#include "RankingManager.h"

class AddRankingState : public Ogre::Singleton<AddRankingState>, public GameState {
    public:
        AddRankingState();
        static AddRankingState& getSingleton ();
        static AddRankingState* getSingletonPtr ();
        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        CEGUI::MouseButton convertMouseButton(const OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);

	void addRecord();
        void setScore(int score);
        void wallpaper();
        bool hide(const CEGUI::EventArgs &e);
        bool accept(const CEGUI::EventArgs &e);
        void updateMenu(float delta);

    private:
        Ogre::Root *_root;
        Ogre::SceneManager *_sceneMgr;
        Ogre::Camera *_camera;
        Ogre::Viewport *_viewport;
        OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        bool _exit;
        int _score;
        CEGUI::Window* _ranking;
        float _time;
};

#endif
