#ifndef CameraManager_H
#define CameraManager_H

#include <Ogre.h>
#include <time.h>       //time
#include <stdlib.h>     //srand, rand

class CameraManager : public Ogre::Singleton<CameraManager> {
    public:
        CameraManager ();
        ~CameraManager () {};

        static CameraManager& getSingleton();
        static CameraManager* getSingletonPtr();

        void update (float delta);

        void addShake (float intensity);

        void nextStage (Ogre::Vector3 looking_at);

        void attack ();

    private:
        enum State {
            newStage,
            inStage,
            attacking
        };
        Ogre::Camera *_camera;
        float _intensity;
        int _angle;
        Ogre::Vector3 _initial_position;
        int _state;
        Ogre::Vector3 _looking_at;
        float _time;
};

#endif
