#ifndef DynamicEntity_H
#define DynamicEntity_H

#include "Entity.h"
#include "Graph.h"
#include "AnimationBlender.h"

class DynamicEntity : public Entity {
    protected:
        float _speed;
        Graph *_graph;
        Ogre::Vector2 _init_position;
        AnimationBlender *_animation;
        int _direction;
        void setDirection (int dir);
    public:
        DynamicEntity (float speed, Ogre::Vector2 position, Ogre::SceneNode *node, Ogre::Entity *entity, Graph *graph) : Entity(position, node, entity), _speed(speed), _graph(graph), _init_position(position), _animation(new AnimationBlender(entity)), _direction(0) {};
        virtual ~DynamicEntity () {};
        virtual void update (float delta) = 0;
        virtual void restartPosition ();
        void setGraph(Graph *graph) { _graph = graph; };
};

#endif
