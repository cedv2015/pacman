#ifndef Enemy_H
#define Enemy_H

#include "DynamicEntity.h"
#include "Node.h"
#include "AnimationBlender.h"
#include <queue>
#include <map>
#include <algorithm>            // std::find
#include <cmath>
#include <iostream>
#include <stdlib.h> 

typedef std::pair<Node*, float> P;

struct Order {
    bool operator()(P const &a, P const &b) const {
        return a.second > b.second;
    }
};

class Enemy : public DynamicEntity {
    protected:
        int _state;
        Node* _nextNode;
        float _epsilon;
        float _width;
        Node *_initialNode;
        float _time_inactive;
        bool InNextNode(float delta);
        void chooseDirection();
        void choosePath();
        void translateToNode();
        AnimationBlender *_animation;
        Node *aStar (Node* start, Node *goal);
        float getDistance (Node *n1, Node *n2);
        Node *_destiny;
        int _type;
        int _colour;
    public:
        Enemy (int type, float speed, Ogre::Vector2 position, Ogre::SceneNode *node, Ogre::Entity *entity, Graph *graph) : DynamicEntity(speed, position, node, entity, graph), _state(0), _epsilon(0.0), _width(0.3), _time_inactive(5), _animation(new AnimationBlender(entity)), _destiny(nullptr), _type(type), _colour(rand() % 4) {};
        ~Enemy () {};
        virtual void update (float delta);
        void setInitialNode(Node *n);
        bool isPlayerColliding (Ogre::Vector2 pos);
        void restartEnemyPosition ();
        Node* getNextNode() { return _nextNode; };
        void attack();
        void run();
        void die();
        void setDestiny (Node *n) { _destiny = n; };
};

#endif
