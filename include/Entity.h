#ifndef Entity_H
#define Entity_H

#include <Ogre.h>

class Entity{
    public:
        Entity (Ogre::Vector2 position, Ogre::SceneNode *node, Ogre::Entity *entity) : _position(position), _node(node), _entity(entity) {};
        virtual ~Entity () {};
    protected:
        Ogre::Vector2 _position;
        Ogre::SceneNode *_node;
        Ogre::Entity *_entity;
};

#endif
