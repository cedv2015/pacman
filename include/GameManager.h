#ifndef GameManager_H
#define GameManager_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <stack>
#include <OgreSingleton.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "InputManager.h"
#include "RankingManager.h"
#include "TrackManager.h"
#include "SoundFXManager.h"

class GameState;

class GameManager : public Ogre::FrameListener, public Ogre::Singleton<GameManager>, public OIS::KeyListener, public OIS::MouseListener {
    public:
        GameManager ();
        static GameManager& getSingleton ();
        static GameManager* getSingletonPtr ();
        ~GameManager ();

        void start (GameState *state);

        void changeState (GameState *state);
        void pushState (GameState *state);
        void popState ();

    protected:
        Ogre::Root * _root;
        Ogre::SceneManager *_sceneManager;
        Ogre::RenderWindow *_renderWindow;

        void loadResources ();
        bool configure ();

        bool frameStarted (const Ogre::FrameEvent &evt);
        bool frameEnded (const Ogre::FrameEvent &evt);

    private:
        bool keyPressed (const OIS::KeyEvent &e);
        bool keyReleased (const OIS::KeyEvent &e);
        bool mouseMoved (const OIS::MouseEvent &e);
        bool mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        bool mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool initSDL ();

        InputManager *_inputMgr;
        std::stack<GameState*> _states;
};

#endif
