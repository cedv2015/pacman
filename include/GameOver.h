#ifndef GameOver_H
#define GameOver_H

#include "StaticEntity.h"
#include "AnimationBlender.h"

class GameOver : public StaticEntity {
    public:
        GameOver ();
        ~GameOver () { delete _animation; };
        bool update (float delta);
    private:
        AnimationBlender *_animation;
};

#endif
