#ifndef Graph_H
#define Graph_H

#include <string>
#include "Node.h"
#include "Wall.h"
#include "Item.h"
#include "ReadyGo.h"
#include "CameraManager.h"

class Enemy;
class Player;
class Graph {
    public:
        void addNode (std::string name, int x, int y);
        void addEdge (std::string name1, std::string name2, int cost);
        void addWall (Wall *wall);
        void addEnemy (Enemy *enemy);
        void addItem (Item *p);
        Graph ();
        ~Graph ();
        bool isPlayerCollidingWithWalls (Ogre::Vector2);
        bool isPlayerCollidingWithEnemies (Ogre::Vector2, int state);
        int isPlayerCollidingWithItems (Ogre::Vector2);
        std::vector<Node*> getNodes();
        std::vector<Wall*> getWalls();
        std::vector<Item*> getItems();
        std::vector<Enemy*> getEnemies(){ return _enemies; };
        Node* getNode(std::string name);
        void setPlayer (Player *player);
        void removeNode (int i);
        void restartEnemies ();
        void reset();
        bool update (float delta);
        void setEnemiesNormal();
        void setTittlePosition(Ogre::Vector3 position) { _readyGo->setPosition(position); };
        Node* getPlayerPosition () { return _player_position; };
        void setPlayerPosition(std::string node);
    private:
        std::vector<Node*> _nodes;
        std::vector<Wall*> _walls;
        std::vector<Item*> _items;
        std::vector<Enemy*> _enemies;
        Player *_player;
        ReadyGo *_readyGo;
        void removeItem (int i);
        Node *_player_position;
};

#endif
