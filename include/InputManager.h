#ifndef InputManager_H
#define InputManager_H

#include <Ogre.h>
#include <OIS/OIS.h>

class InputManager : public Ogre::Singleton<InputManager>, public OIS::KeyListener, public OIS::MouseListener {
    public:
        InputManager ();
        virtual ~InputManager ();

        void initialise (Ogre::RenderWindow *renderWindow);
        void capture ();

        void addKeyListener (OIS::KeyListener *keyListener, const std::string &instanceName);
        void addMouseListener (OIS::MouseListener *keyListener, const std::string &instanceName);

        void removeKeyListener (const std::string &name);
        void removeMouseListener (const std::string &name);
        void removeKeyListener (OIS::KeyListener *keyListener);
        void removeMouseListener (OIS::MouseListener *mouseListener);

        OIS::Keyboard* getKeyboard ();
        OIS::Mouse* getMouse ();

        static InputManager& getSingleton ();
        static InputManager* getSingletonPtr ();

        bool frameStarted (const Ogre::FrameEvent &evt);
        bool frameEnded (const Ogre::FrameEvent &evt);

    private:
        bool keyPressed (const OIS::KeyEvent &e);
        bool keyReleased (const OIS::KeyEvent &e);
        bool mouseMoved (const OIS::MouseEvent &e);
        bool mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        bool mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        OIS::InputManager *_inputSystem;
        OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        std::map<std::string, OIS::KeyListener*> _keyListeners;
        std::map<std::string, OIS::MouseListener*> _mouseListeners;
        std::map<std::string, OIS::KeyListener*>::iterator _itKeyListener;
        std::map<std::string, OIS::MouseListener*>::iterator _itMouseListener;
        std::map<std::string, OIS::KeyListener*>::iterator _itKeyListenerEnd;
        std::map<std::string, OIS::MouseListener*>::iterator _itMouseListenerEnd;
};

#endif
