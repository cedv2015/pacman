#ifndef InstructionsState_H
#define InstructionsState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "PlayState.h"
#include "MenuState.h"

class InstructionsState : public Ogre::Singleton<InstructionsState>, public GameState {
    public:
        InstructionsState ();
        static InstructionsState& getSingleton ();
        static InstructionsState* getSingletonPtr ();
        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        CEGUI::MouseButton convertMouseButton(const OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);

        void instructionsShow();
        void wallpaper();
        bool hide(const CEGUI::EventArgs &e);
        bool previous(const CEGUI::EventArgs &e);
        bool next(const CEGUI::EventArgs &e);
        void updateMenu(float delta);

    private:
  	    Ogre::Root* _root;
  	    Ogre::SceneManager* _sceneMgr;
  	    Ogre::Viewport* _viewport;
  	    Ogre::Camera* _camera;

  	    OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        bool _exit;
        CEGUI::Window* _instructions;
        int _page;
        float _time;
};

#endif
