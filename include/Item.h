#ifndef Item_H
#define Item_H

#include "StaticEntity.h"
#include "AnimationBlender.h"
#include <string.h>

class Item : public StaticEntity {
    public:
        Item (Ogre::Vector2 position, Ogre::SceneNode *node, Ogre::Entity *entity, std::string type) : StaticEntity(position, node, entity), _type(type), _width(0.4f), _done(false), _taken(false), _animation(new AnimationBlender(entity)) {};
        virtual ~Item () { delete _animation; };
        virtual void take () = 0;
        int isPlayerColliding (Ogre::Vector2 vec);
        void update (float delta);
        bool isDone () { return _done; };
        bool isTaken () { return _taken; };
    protected:
        std::string _type;
        float _width;
        bool _done, _taken;
        AnimationBlender *_animation;

};

#endif
