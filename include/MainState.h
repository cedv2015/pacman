#ifndef MainState_H
#define MainState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "GameState.h"
#include "Reader.h"
#include "MenuState.h"

class MainState : public Ogre::Singleton<MainState>, public GameState {
    public:
        MainState();
        static MainState& getSingleton ();
        static MainState* getSingletonPtr ();
        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);

    private:
        Ogre::Root *_root;
        Ogre::SceneManager *_sceneMgr;
        Ogre::Camera *_camera;
        Ogre::Viewport *_viewport;
        OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        bool _exit;
        Reader *_reader;
};

#endif
