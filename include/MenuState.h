#ifndef MenuState_H
#define MenuState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "PlayState.h"
#include "RankingState.h"
#include "InstructionsState.h"

class MenuState : public Ogre::Singleton<MenuState>, public GameState {
    public:
        MenuState ();
        static MenuState& getSingleton ();
        static MenuState* getSingletonPtr ();
        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        CEGUI::MouseButton convertMouseButton(const OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);

        void menu();
        void updateMenu(float delta);

        //CEGUI
        void createGUI();
        bool play(const CEGUI::EventArgs &e);
        bool quit(const CEGUI::EventArgs &e);
        bool instructions(const CEGUI::EventArgs &e);
        bool ranking(const CEGUI::EventArgs &e);
        bool hide(const CEGUI::EventArgs &e);

    private:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;


        OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        bool _exit;
        CEGUI::OgreRenderer* _renderer;
        CEGUI::Window *_sheet, *_instructions;
        float _time;
};

#endif
