#ifndef Node_H
#define Node_H

#include <vector>
#include <utility>      //pair
#include <time.h>       //time
#include <stdlib.h>     //srand, rand
#include <string>
#include <Ogre.h>

class Node {
    public:
        void addEdge(int cost, int position, Node* n);
        std::string getName () { return _name; };
        int getX () { return _x; };
        int getY () { return _y; };
        Node (std::string name, int x, int y): _name(name), _up(std::make_pair(nullptr, 0)), _down(std::make_pair(nullptr, 0)) , _left(std::make_pair(nullptr, 0)), _right(std::make_pair(nullptr, 0)), _x(x), _y(y) { srand(time(NULL)); };
        ~Node () {};
        Node* selectRandomAdyacent ();
        Ogre::Vector3 getPosition () { return Ogre::Vector3(_x, 0, -_y); };
        bool isEdge ();
        void replaceEdge (int cost, int position, Node* n);
        int getCost (Node *node);
        std::vector<Node*> getAdyacents ();
    private:
        std::string _name;
        std::pair<Node*, int> _up;
        std::pair<Node*, int> _down;
        std::pair<Node*, int> _left;
        std::pair<Node*, int> _right;
        int _x, _y;
};

#endif
