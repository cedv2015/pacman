#ifndef PauseState_H
#define PauseState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "GameState.h"
#include "PlayState.h"
#include "MenuState.h"

class PauseState : public Ogre::Singleton<PauseState>, public GameState {
    public:
        PauseState ();

        static PauseState &getSingleton ();
        static PauseState *getSingletonPtr ();

        void enter();
        void exit();
        void pause();
        void resume();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        CEGUI::MouseButton convertMouseButton(const OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);

        void pauseCEGUI();
        bool quit(const CEGUI::EventArgs &e);
        bool menu(const CEGUI::EventArgs &e);
        bool play(const CEGUI::EventArgs &e);

    private:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;


        OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        bool _exit;
        CEGUI::Window *_pauseMenu;
};

#endif

