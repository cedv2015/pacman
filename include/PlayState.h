#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "GameState.h"
#include "Player.h"
#include "Enemy.h"
#include "RankingManager.h"
#include "AddRankingState.h"
#include "CameraManager.h"
#include "PauseState.h"
#include "GameOver.h"
#include "Reader.h"

class PlayState : public Ogre::Singleton<PlayState>, public GameState {
    public:
        PlayState ();
        static PlayState& getSingleton ();
        static PlayState* getSingletonPtr ();

        void enter();
        void exit();
        void pause();
        void resume();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);

        void positionCamera();
	    void showOverlay();
        void lifes();
        void reset();
    private:
        Ogre::Root *_root;
        Ogre::SceneManager *_sceneMgr;
        Ogre::Camera *_camera;
        Ogre::Viewport *_viewport;
        OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        bool _exit;
        Player *_player;
        std::vector<Enemy*> _enemies;
        RankingManager *_rankingMgr;
        CEGUI::Window *_overlay, *_lifes;
        CameraManager *_camera_manager;
        GameOver *_gameOver;
};

#endif
