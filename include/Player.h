#ifndef Player_H
#define Player_H

#include "DynamicEntity.h"
#include "SoundFXManager.h"
#include <cmath>

class Player : public DynamicEntity {
    private:
        int _state;
        int _life;
        int _score;
        float _timer;
        float _time_limit;
        bool _up, _down, _left, _right;
        int _stage_width;
        float _time_attack;
        SoundFXPtr _coinSound;
        SoundFXPtr _powerUp;
        SoundFXPtr _dead;
        bool checkCollision ();
        bool checkEnemies ();
        void checkItems ();
        void die ();
        void addTimeScore ();
        void checkTeleport ();
        void setAnimation (const Ogre::String &animation, Ogre::Real duration);
    public:
        Player (float speed, Ogre::Vector2 position, Ogre::SceneNode *node, Ogre::Entity *entity, Graph *graph, float stage_width);
        virtual ~Player () {};

        virtual void update (float delta);
        void pressUp () { _up = true; };
        void pressDown () { _down = true; };
        void pressLeft () { _left = true; };
        void pressRight () { _right = true; };
        void releaseUp () { _up = false; };
        void releaseDown () { _down = false; };
        void releaseLeft () { _left = false; };
        void releaseRight () { _right = false; };
        void releaseAll () { _up = _down = _left = _right = false; };
        float getTime () { return _timer; };
        float getScore () { return _score; };
        void resetTime () { _timer = 0; };
        void resetScore () { _score = 0; };
        void nextStage (int x, int y, Graph *graph, Ogre::SceneNode *node, Ogre::Entity *ent, float time_limit, int stage_width);
        bool isDead() { return _life < 0; };
        int getLife() { return _life; };
        void restartLifes() { _life = 3; releaseAll(); };
        enum State {
            Run,
            Iddle,
            Attack
        };
};

#endif
