#ifndef Point_H
#define Point_H

#include "Item.h"

class Point : public Item {
    public:
        Point (Ogre::Vector2 position, Ogre::SceneNode *node, Ogre::Entity *entity) : Item(position, node, entity, std::string("point")) {};
        ~Point () {};
        virtual void take ();
};

#endif
