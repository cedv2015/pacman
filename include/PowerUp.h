#ifndef PowerUp_H
#define PowerUp_H

#include "Item.h"

class PowerUp : public Item {
    public:
        PowerUp (Ogre::Vector2 position, Ogre::SceneNode *node, Ogre::Entity *entity) : Item(position, node, entity, std::string("powerUp")) {};
        ~PowerUp () {};
        virtual void take ();
};

#endif
