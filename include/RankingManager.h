#ifndef RankingManager_H
#define RankingManager_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <Ogre.h>


class RankingManager : public Ogre::Singleton<RankingManager> {
    public:
        RankingManager () {};
        ~RankingManager () {};

        static RankingManager& getSingleton ();
        static RankingManager* getSingletonPtr ();
 
        std::vector<std::string> getRanking ();
        bool checkRanking (int turns);
        void setRanking (std::string, int turns);

    private:
        std::vector<std::string> _rankings;
        void saveRankings ();
        void loadRankings ();
};

#endif
