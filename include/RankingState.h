#ifndef RankingState_H
#define RankingState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "PlayState.h"
#include "MenuState.h"
#include "RankingManager.h"

class RankingState : public Ogre::Singleton<RankingState>, public GameState {
    public:
        RankingState ();
        static RankingState& getSingleton ();
        static RankingState* getSingletonPtr ();
        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
	    CEGUI::MouseButton convertMouseButton(const OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);

        void rankingShow();
        void wallpaper();
        void updateMenu(float delta);
        bool hide(const CEGUI::EventArgs &e);

    private:
	Ogre::Root* _root;
	Ogre::SceneManager* _sceneMgr;
	Ogre::Viewport* _viewport;
	Ogre::Camera* _camera;

	RankingManager *_rankingManager;
        OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        bool _exit;
        CEGUI::Window* _records;
        float _time;
};

#endif
