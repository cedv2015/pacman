#ifndef Reader_H
#define Reader_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <OgreSingleton.h>

#include "Wall.h"
#include "Point.h"
#include "Enemy.h"
#include "Player.h"
#include "Graph.h"
#include "PowerUp.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstring>

class Reader: public Ogre::Singleton<Reader>{
    private:
        int _cols, _rows;
        int wall=0, point=0, enemy=0, player=0, powerup=0;
        std::vector<int> _matrix;
        Ogre::SceneManager* _sceneManager;
        Graph* _graph;
        Player *_player;
        Ogre::SceneNode* _stage;
        std::vector<Enemy*> _enemies;
        int _level;
        float _speed;
        bool _gameOver;
        void addFloor ();
    public:
        Reader ();
        ~Reader ();
        static Reader& getSingleton(void);
        static Reader* getSingletonPtr(void);
        void convertMatrix (std::string num);
        void read ();
        void createGraph();
        void optimizeGraph();
        Player* getPlayer();
        std::vector<Enemy*> getEnemies();
        int getCols();
        int getRows();
        int getLevel();
        void destroyStage();
        void loadNextStage ();
	std::vector<int> transformateMatrix();
        void wallpaper();
        void resetLevel();
        bool isGameOver() { return _gameOver; };
};

#endif
