#ifndef ReadyGo_H
#define ReadyGo_H

#include "StaticEntity.h"
#include "AnimationBlender.h"

class ReadyGo : public StaticEntity {
    public:
        ReadyGo (Ogre::Vector2 position, Ogre::SceneNode *node, Ogre::Entity *entity);
        ~ReadyGo () { delete _animation; };
        bool update (float delta);
        void reset (Ogre::SceneNode *node, Ogre::Entity *entity);
        void setPosition(Ogre::Vector3 position);
    private:
        AnimationBlender *_animation;
        float _time;
};

#endif
