#ifndef StaticEntity_H
#define StaticEntity_H

#include "Entity.h"

class StaticEntity : public Entity {
    public:
        StaticEntity (Ogre::Vector2 position, Ogre::SceneNode *node, Ogre::Entity *entity) : Entity(position, node, entity) {};
        virtual ~StaticEntity () {};
};

#endif
