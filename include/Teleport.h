#ifndef Teleport_H
#define Teleport_H

#include "StaticEntity.h"

class Teleport : public StaticEntity {
    public:
        Teleport (Ogre::Vector2 position, Ogre::SceneNode *node, Ogre::Entity *entity) : StaticEntity(position, node, entity) {};
        ~Teleport () {};
        void SetDestiny (Teleport *destiny) { _destiny = destiny; };
    private:
        Teleport *_destiny;
};

#endif
