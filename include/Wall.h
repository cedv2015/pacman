#ifndef Wall_H
#define Wall_H

#include "StaticEntity.h"

class Wall : public StaticEntity {
    public:
        Wall (Ogre::Vector2 position, Ogre::SceneNode *node, Ogre::Entity *entity) : StaticEntity(position, node, entity), _width(0.45f) {};
        ~Wall () {};
        bool isPlayerColliding (Ogre::Vector2 vec);
    protected:
        Ogre::Real _width;
};

#endif
