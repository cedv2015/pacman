#include "AddRankingState.h"

template<> AddRankingState* Ogre::Singleton<AddRankingState>::msSingleton = nullptr;

AddRankingState::AddRankingState () : _root(nullptr), _sceneMgr(nullptr), _camera(nullptr), _keyboard(nullptr), _mouse(nullptr), _exit(false), _ranking(nullptr), _time(0) {
    _root = Ogre::Root::getSingletonPtr();
}

AddRankingState* AddRankingState::getSingletonPtr () {
    return msSingleton;
}

AddRankingState& AddRankingState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void AddRankingState::enter () {
    std::cout << "Entered in AddRankingState" << std::endl;
    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    wallpaper();
    addRecord();
}

void AddRankingState::exit() {
	_sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void AddRankingState::pause () {}

void AddRankingState::resume () {}

bool AddRankingState::frameStarted (const Ogre::FrameEvent &evt) {
    updateMenu(evt.timeSinceLastFrame);
    return true;
}

bool AddRankingState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
}

void AddRankingState::keyPressed(const OIS::KeyEvent& evt)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(evt.key));
  CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(evt.text);

}

void AddRankingState::keyReleased(const OIS::KeyEvent& evt)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp(static_cast<CEGUI::Key::Scan>(evt.key));

}

void AddRankingState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}
void AddRankingState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}
void AddRankingState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

CEGUI::MouseButton AddRankingState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}

void AddRankingState::addRecord () {

    std::string name = "";
    if(_ranking == NULL){
        _ranking = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("addRecords.layout");
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_ranking);
        CEGUI::Window* frameWindow = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->getChild("AddRanking");
        frameWindow->setPosition(CEGUI::UVector2(CEGUI::UDim(0.25,0),CEGUI::UDim(0.1,0)));
        CEGUI::Window* exitButton = _ranking->getChild("ExitRecord");
        exitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&AddRankingState::hide, this));
        CEGUI::Window* acceptButton = _ranking->getChild("AcceptRecord");
        acceptButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&AddRankingState::accept, this));




    }else{
        _ranking->show();
    }

}

void AddRankingState::setScore(int score){
    _score = score;
}

bool AddRankingState::hide(const CEGUI::EventArgs &e)
{
    _ranking->hide();
    changeState(RankingState::getSingletonPtr());
    return true;

}

bool AddRankingState::accept(const CEGUI::EventArgs &e)
{
    CEGUI::Window* name_record = _ranking->getChild("NameBox");
    std::string name = name_record->getText().c_str();
    RankingManager::getSingletonPtr()->setRanking(name, _score);
    _ranking->hide();
    changeState(RankingState::getSingletonPtr());
    return true;

}

void AddRankingState::wallpaper(){

    _sceneMgr->getCamera("MainCamera")->setPosition(Ogre::Vector3(0, 0, 10));
    _sceneMgr->getCamera("MainCamera")->lookAt(Ogre::Vector3(0, 0, 8));
    /*_camera->setPosition(Ogre::Vector3(0,20,0));
    _camera->lookAt(Ogre::Vector3(0,0,0));
    _camera->setNearClipDistance(0.1);
    _camera->setFarClipDistance(100);*/

    Ogre::TexturePtr m_backgroundTexture = Ogre::TextureManager::getSingleton().createManual("BackgroundTexture",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D,640, 480, 0, Ogre::PF_BYTE_BGR,Ogre::TU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
    Ogre::Image m_backgroundImage;
    m_backgroundImage.load("fondo-piedras-claro.png",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    m_backgroundTexture->loadImage(m_backgroundImage);
    Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("BackgroundMaterial", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    material->getTechnique(0)->getPass(0)->createTextureUnitState("BackgroundTexture");
    material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    material->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(-0.05, 0.0);
    Ogre::Rectangle2D* rect = new Ogre::Rectangle2D(true);
    rect->setCorners(-1.0, 1.0, 1.0, -1.0);
    rect->setMaterial("BackgroundMaterial");
    rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
    rect->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    Ogre::SceneNode* headNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundAddRanking");
    headNode->attachObject(rect);

    Ogre::Rectangle2D *rectRails = new Ogre::Rectangle2D(true);
    rectRails->setCorners(-1.0, -0.5, 1.0, -1.0);
    rectRails->setBoundingBox(Ogre::AxisAlignedBox(-100000.0f * Ogre::Vector3::UNIT_SCALE, 100000.0f * Ogre::Vector3::UNIT_SCALE));
    Ogre::MaterialPtr materialRails = Ogre::MaterialManager::getSingleton().getByName("MaterialRails");
    materialRails->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    materialRails->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    materialRails->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    rectRails->setMaterial("MaterialRails");
    materialRails->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(0.15, 0.0);
    rectRails->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
    rectRails->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    Ogre::SceneNode* railsNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundRailsAddRanking");
    railsNode->attachObject(rectRails);

    Ogre::Entity *ent_player = _sceneMgr->createEntity("Player_AddRanking_rails", "Player_rails.mesh");
    Ogre::SceneNode *node_player = _sceneMgr->createSceneNode("Player_rails_AddRanking_Node");
    _sceneMgr->getRootSceneNode()->addChild(node_player);
    node_player->attachObject(ent_player);
    node_player->setPosition(Ogre::Vector3(3.5,-4,0));
    node_player->scale(Ogre::Vector3(1,1,1));
    node_player->roll(Ogre::Degree(180));
    node_player->pitch(Ogre::Degree(180));
    node_player->yaw(Ogre::Degree(90));
    node_player->translate(Ogre::Vector3(0,1.5,0));
    ent_player->setMaterialName("MaterialPlayer");
}

  void AddRankingState::updateMenu(float delta){
    _time += delta;
    _sceneMgr->getSceneNode("Player_rails_AddRanking_Node")->setPosition(Ogre::Vector3(3.5,Ogre::Math::Sin(_time*10)*0.05-2.6,0));
}
