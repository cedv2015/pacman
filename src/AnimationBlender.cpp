#include "AnimationBlender.h"

AnimationBlender::AnimationBlender(Ogre::Entity *entity) : mEntity(entity) {
    Ogre::AnimationStateSet *set = mEntity->getAllAnimationStates();
    Ogre::AnimationStateIterator it = set->getAnimationStateIterator();
  // Inicializamos los AnimationState de la entidad
    while(it.hasMoreElements()) {
        Ogre::AnimationState *anim = it.getNext();
        anim->setEnabled(false);
        anim->setWeight(0);
        anim->setTimePosition(0);
    }
    mSource = nullptr;  mTarget = nullptr;  mTimeleft = 0;
}

void AnimationBlender::blend (const Ogre::String &animation, 
			      BlendingTransition transition, 
			      Ogre::Real duration, bool l) {
    Ogre::AnimationState *newTarget = mEntity->getAnimationState(animation);
    newTarget->setLoop(l);
    mComplete = false;
    mTransition = transition;
    mDuration = duration;
    mLoop = l;
 
    if ((mTimeleft <= 0) || (transition == AnimationBlender::Switch)){
        if (mSource != nullptr) mSource->setEnabled(false);
        mSource = newTarget;
        mSource->setEnabled(true);
        mSource->setWeight(1);
        mSource->setTimePosition(0);
        mTimeleft = mSource->getLength(); 
        mTarget = nullptr;
    }
    else {
        if (mSource != newTarget) {
            mTarget = newTarget;
            mTarget->setEnabled(true);
            mTarget->setWeight(0);
            mTarget->setTimePosition(0);
        }
    }
}

void AnimationBlender::addTime(Ogre::Real time) {
    if (mSource == nullptr) return;   // No hay fuente
    mSource->addTime(time);   mComplete = false;
    mTimeleft -= time;
    if ((mTimeleft <= 0) && (mTarget == nullptr)) mComplete = true;

    if (mTarget != nullptr) {  // Si hay destino
        if (mTimeleft <= 0) {  
            mSource->setEnabled(false);  mSource->setWeight(0);
            mSource = mTarget;
            mSource->setEnabled(true);   mSource->setWeight(1);
            mTimeleft = mSource->getLength();
            mTarget = nullptr;
        }
    else {   // Queda tiempo en Source... cambiar pesos
        Ogre::Real weight = mTimeleft / mDuration;
        if (weight > 1) weight = 1.0;
            mSource->setWeight(weight);
            mTarget->setWeight(1.0 - weight);
            if (mTransition == AnimationBlender::Blend) mTarget->addTime(time);
        }
    }
    if ((mTimeleft <= 0) && mLoop) mTimeleft = mSource->getLength();
}


