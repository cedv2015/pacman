#include "CameraManager.h"

template <> CameraManager* Ogre::Singleton<CameraManager>::msSingleton = nullptr;

CameraManager::CameraManager () : _camera(nullptr), _intensity(0), _angle(0), _initial_position(Ogre::Vector3(0,0,0)), _state(newStage), _looking_at(Ogre::Vector3(0,0,0)), _time(0) {
    srand(time(NULL));
    _camera = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager")->getCamera("MainCamera");
};

CameraManager* CameraManager::getSingletonPtr() {
    return msSingleton;
}

CameraManager& CameraManager::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void CameraManager::update (float delta) {
    Ogre::Vector3 actualPosition = _initial_position;
    if (_state == newStage) {
        _intensity -= delta;
        if (_intensity <= 0) {
            _state = inStage;
            _camera->setPosition(_initial_position);
            _camera->lookAt(_looking_at);
        }
        actualPosition = Ogre::Vector3(_initial_position.x - _intensity * 0.4f * _initial_position.x, _initial_position.y * 1.0f, _initial_position.z - _intensity * 0.4f * _initial_position.z);
    }
    else {
        if (_state == inStage) {
            _intensity -= delta / 2;
        }
        else if (_state == attacking) {
            _time -= delta;
            if (_time < 0) {
                _state = inStage;
            }
        }
        if (_intensity > 0){
            actualPosition = Ogre::Vector3(_initial_position.x + _intensity * 0.2f * Ogre::Math::Cos(Ogre::Math::DegreesToRadians(_angle)) * Ogre::Math::Sin(_time * 20), _initial_position.y, _initial_position.z + _intensity * 0.2f * Ogre::Math::Sin(Ogre::Math::DegreesToRadians(_angle)) * Ogre::Math::Sin(_time * 20));
        }
    }
    _camera->setPosition(actualPosition);
    _camera->lookAt(_looking_at);
}

void CameraManager::addShake (float intensity) {
    _intensity = intensity;
    _angle = rand() % 360;
}

void CameraManager::nextStage (Ogre::Vector3 looking_at) {
    _initial_position = _camera->getPosition();
    if (!looking_at.isZeroLength()) {
        _looking_at = looking_at;
    }
    _intensity = 2;
    _angle = 220;
    _state = newStage;
}

void CameraManager::attack () {
    _state = attacking;
    _intensity = 3;
    _time = 5;
    _angle = rand() % 360;
}
