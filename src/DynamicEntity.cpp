#include "DynamicEntity.h"

void DynamicEntity::setDirection (int dir) {
    float rotation = dir * 90;
    _node->resetOrientation();
    _node->yaw(Ogre::Degree(rotation));
}

void DynamicEntity::restartPosition () {
    _position = _init_position;
    _node->setPosition (Ogre::Vector3 (_position.x, 0, -_position.y));
}
