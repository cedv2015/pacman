#include "Enemy.h"
#include <iostream>

void Enemy::update(float delta) {
    _time_inactive += delta;
    if (_time_inactive >= 5) {
        if (InNextNode(delta)) {
            translateToNode();
            choosePath();
        }
        Ogre::Vector2 distance (0,0);
        switch (_direction) {
            case 0:
                distance.y = _speed * delta;
                break;
            case 1:
                distance.x = _speed * delta;
                break;
            case 2:
                distance.y = -_speed * delta;
                break;
            case 3:
                distance.x = -_speed * delta;
                break;
            default:
                break;
        }
        _position += distance;
        _node->translate(Ogre::Vector3(distance.x, 0, -distance.y));
        _animation->addTime(delta);
    }
}

void Enemy::chooseDirection () {
    if (_nextNode->getX() > _position.x) {
        _direction = 1;
    }
    else if (_nextNode->getX() < _position.x) {
        _direction = 3;
    }
    else if (_nextNode->getY() > _position.y) {
        _direction = 0;
    }
    else {
        _direction = 2;
    }
    setDirection(_direction % 2 == 0 ? _direction + 2 : _direction);
}

void Enemy::choosePath () {
    if (_type == 0) {
        Node *n_aux = _nextNode->selectRandomAdyacent();
        if (_nextNode->getCost(n_aux) == 0) {
            _nextNode = n_aux;
            translateToNode();
            choosePath();
        }
        _nextNode = n_aux;
    }
    else {
        Node *n_aux = _nextNode;
        _nextNode = aStar(n_aux, _destiny);
        if (n_aux == _nextNode) {
            _nextNode = n_aux->selectRandomAdyacent();
        }
        if (n_aux->getCost (_nextNode) == 0) {
            translateToNode();
            choosePath();
            return;
        }
    }
    this->chooseDirection();
}

Node* Enemy::aStar (Node *start, Node *goal) {
    std::vector<Node*> closed;
    float f = getDistance(start, goal);
    //TODO: map with path
    std::map<Node*, Node*> path;
    std::priority_queue<P, std::vector<P>, Order> queue;
    queue.push(std::make_pair(start, f));

    while (queue.size() > 0) {
        Node *top = queue.top().first;
        float cost = queue.top().second;
        queue.pop();
        if (top == goal) {
            //TODO: return path
            Node *sol = top;
            if (path.size() == 0) {
                return sol;
            }
            while(path[sol] != start) {
                sol = path[sol];
            }
            return sol;
        }
        closed.push_back(top);
        for (Node* next : top->getAdyacents()) {
            if (std::find(closed.begin(), closed.end(), next) == closed.end()){
                float g = cost + top->getCost(next);
                float h = g + getDistance(next, goal);
                //TODO: add to path
                path[next] = top;
                queue.push(std::make_pair(next, h));
            }
        }
    }
    return nullptr;
}

float Enemy::getDistance (Node *n1, Node *n2) {
    return 0;
    float f = std::sqrt(float(((n1->getX() - n2->getX())^2) + ((n1->getY() - n2->getY())^2)));
    return f;
}

bool Enemy::InNextNode (float delta) {
    _epsilon = delta * _speed;
    if ((std::abs (_position.x - _nextNode->getX()) <= _epsilon) && (std::abs (_position.y - _nextNode->getY()) <= _epsilon)) {
        return true;
    }
    return false;
}

void Enemy::translateToNode () {
    _position.x = _nextNode->getX();
    _position.y = _nextNode->getY();
    _node->setPosition(_nextNode->getPosition());
}

void Enemy::setInitialNode(Node *n){
    _nextNode = n;
    _initialNode = _nextNode;
    this->chooseDirection();
}

bool Enemy::isPlayerColliding (Ogre::Vector2 pos) {
    if (_time_inactive >= 5) {
        bool doNotcollidesX = pos.x - _width >= _position.x + _width || pos.x + _width <= _position.x - _width;
        bool doNotcollidesY = pos.y - _width >= _position.y + _width || pos.y + _width <= _position.y - _width;
        if (!doNotcollidesX && !doNotcollidesY) {
            return true;
        }
    }
    return false;
}

void Enemy::restartEnemyPosition () {
    this->restartPosition();
    this->setInitialNode (_initialNode);
}

void Enemy::attack () {
    _animation->blend("Attack", AnimationBlender::Switch, 0.5, true);
    _entity->setMaterialName("Enemy");
}

void Enemy::run () {
    _animation->blend("Run", AnimationBlender::Switch, 0.5, true);
    _entity->setMaterialName("Enemy_run");
}

void Enemy::die () {
    _time_inactive = 0;
    restartEnemyPosition ();
}
