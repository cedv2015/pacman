#include "GameOver.h"

GameOver::GameOver () : StaticEntity(Ogre::Vector2(0,0), nullptr, nullptr) {
    Ogre::SceneManager *sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
    _node = sceneMgr->createSceneNode("GameOver");
    _entity = sceneMgr->createEntity("GameOver", "GameOver.mesh");
    _node->attachObject(_entity);
    sceneMgr->getRootSceneNode()->addChild(_node);
    Ogre::Vector3 position = sceneMgr->getCamera("MainCamera")->getPosition();
    position.y -= 6;
    position.z -= 6;
    _node->setPosition(position);
    _node->resetOrientation();
    _node->yaw(Ogre::Degree(-90));
    _node->roll(Ogre::Degree(45));
    _animation = new AnimationBlender(_entity);
    _animation->blend("Slide", AnimationBlender::Switch, 3, false);
}

bool GameOver::update (float delta) {
    if (_animation->mComplete) {
        _node->setVisible(false);
        return true;
    }
    _animation->addTime(delta);
    return false;
}

