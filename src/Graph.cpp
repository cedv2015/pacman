#include "Graph.h"
#include "Enemy.h"
#include "Player.h"
#include "Reader.h"

Graph::Graph () : _player(nullptr), _player_position(nullptr) {
    Ogre::SceneManager *sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
    Ogre::Entity *ent = sceneMgr->createEntity("ReadyGo", "ReadyGo.mesh");
    Ogre::SceneNode *node = sceneMgr->createSceneNode("ReadyGo");
    sceneMgr->getRootSceneNode()->addChild(node);
    node->attachObject(ent);
    node->setPosition(0, 3, 0);
    _readyGo = new ReadyGo(Ogre::Vector2(0,0), node, ent);
}

Graph::~Graph () {
    delete _readyGo;
    for (Wall* w : _walls) {
        delete w;
    }
    for (Node* n : _nodes) {
        delete n;
    }
    for (Item *i : _items) {
        delete i;
    }
    for (Enemy *e : _enemies) {
        delete e;
    }
}

void Graph::addNode (std::string name, int x, int y) {
    Node *n = new Node (name, -x, -y);
    _nodes.push_back(n);
}

void Graph::addEdge (std::string name1, std::string name2, int cost) {
    Node *n1 = nullptr;
    Node *n2 = nullptr;
    for (Node *n : _nodes) {
        if (name1 == n->getName()) {
            n1 = n;
        }
        else if (name2 == n->getName()) {
            n2 = n;
        }
        if (n1 != nullptr && n2 != nullptr) {
            break;
        }
    }

    if (n1 == nullptr || n2 == nullptr) {
        return;
    }

    if (cost == 0) {
        n1->addEdge(cost, 1, n2);
        n2->addEdge(cost, 3, n1);
    }
    else if (n1->getX() == n2->getX()) {
        // vertically aligned
        if (n1->getY() > n2->getY()) {
            // n1 over n2
            int cost = n1->getY() - n2->getY();
            n1->addEdge(cost, 2, n2);
            n2->addEdge(cost, 0, n1);
        }
        else {
            // n2 over n1
            int cost = n2->getY() - n1->getY();
            n1->addEdge(cost, 0, n2);
            n2->addEdge(cost, 2, n1);
        }
    }
    else if (n1->getY() == n2->getY()) {
        // horizontally aligned
         if (n1->getX() > n2->getX()) {
            // n1 to the right of n2
            int cost = n1->getX() - n2->getX();
            n1->addEdge(cost, 3, n2);
            n2->addEdge(cost, 1, n1);
        }
        else {
            // n2 to the right of n1
            int cost = n2->getX() - n1->getX();
            n1->addEdge(cost, 1, n2);
            n2->addEdge(cost, 3, n1);
        }
    }
}

void Graph::addWall (Wall* wall) {
    _walls.push_back(wall);
}

void Graph::addEnemy (Enemy *enemy) {
    _enemies.push_back(enemy);
}

void Graph::addItem (Item *item) {
    _items.push_back(item);
}

void Graph::setPlayer (Player *player) {
    _player = player;
}

bool Graph::isPlayerCollidingWithWalls (Ogre::Vector2 vec) {
    for (Wall *w : _walls) {
        if (w->isPlayerColliding (vec) == true) {
            return true;
        }
    }
    return false;
}

bool Graph::isPlayerCollidingWithEnemies (Ogre::Vector2 vec, int state) {
    for (Enemy *e : _enemies) {
        if (e->isPlayerColliding (vec) == true) {
            if (state == Player::Attack) {
                e->die();
            }
            return true;
        }
    }
    return false;
}

int Graph::isPlayerCollidingWithItems (Ogre::Vector2 vec) {
    for (unsigned int i = 0; i<_items.size(); i++) {
        Item *item = _items[i];
        if (!item->isTaken()){
            int type = item->isPlayerColliding (vec);
            if (type != 0) {
                item->take();
                if (type == 1) {
                    for (Enemy *e : _enemies) {
                        e->run();
                    }
                }
                return type;
            }
        }
    }
    return 0;
}

std::vector<Node*> Graph::getNodes(){
    return _nodes;
}

std::vector<Wall*> Graph::getWalls(){
    return _walls;
}

std::vector<Item*> Graph::getItems(){
    return _items;
}

Node* Graph::getNode(std::string name){
    for (Node *n : _nodes) {
        if (n->getName() == name) {
            return n;
        }
    }
    return nullptr;
}

void Graph::removeNode(int i) {
    Node *n;
    n = _nodes[i];
    if (n == _player_position) {
        _player_position = n->selectRandomAdyacent ();
        for (Enemy *e : _enemies) {
            e->setDestiny(_player_position);
        }
    }
    _nodes.erase(_nodes.begin()+i);
    delete n;
}

void Graph::restartEnemies () {
    _readyGo->reset(nullptr, nullptr);
    CameraManager::getSingletonPtr()->nextStage(Ogre::Vector3(0,0,0));

    for (Enemy *e : _enemies) {
        e->restartEnemyPosition ();
        e->attack();
    }
}

void Graph::removeItem (int i) {
    Item *item = _items[i];
    _items.erase(_items.begin()+i);
    delete item;
    if (_items.size() <= 0) {
        Reader::getSingletonPtr ()->loadNextStage();
    }
}

void Graph::reset(){
    for (Wall *w : _walls) {
        delete w;
    }
    _walls.clear();
    for (Node *w : _nodes) {
        delete w;
    }
    _nodes.clear();
    for (Item *w : _items) {
        delete w;
    }
    _items.clear();
    for (Enemy *w : _enemies) {
        delete w;
    }
    _enemies.clear();
    Ogre::SceneManager *sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
    Ogre::Entity *ent = sceneMgr->createEntity("ReadyGo", "ReadyGo.mesh");
    Ogre::SceneNode *node = sceneMgr->createSceneNode("ReadyGo");
    sceneMgr->getRootSceneNode()->addChild(node);
    node->attachObject(ent);
    node->setPosition(0, 3, 0);
    _readyGo->reset(node, ent);
    CameraManager::getSingletonPtr()->nextStage(Ogre::Vector3(0,0,0));

}

bool Graph::update (float delta) {
    bool isReady = _readyGo->update(delta);
    if (isReady) {
        for (Enemy *e : _enemies){
            e->update(delta);
        }
        for (unsigned int i = 0; i < _items.size(); i++) {
            Item *item = _items[i];
            item->update(delta);
            if (item->isDone() == true) {
                removeItem(i);
            }
        }
    }
    CameraManager::getSingletonPtr()->update(delta);
    return isReady;
}

void Graph::setEnemiesNormal () {
    for (Enemy *e : _enemies) {
        e->attack();
    }
}

void Graph::setPlayerPosition (std::string node) {
    if (Node * n = getNode (node)) {
        _player_position = n;
        for (Enemy *e : _enemies) {
            e->setDestiny(n);
        }
    }
}
