#include "InputManager.h"

template<> InputManager* Ogre::Singleton<InputManager>::msSingleton = nullptr;

InputManager::InputManager () : _inputSystem(nullptr), _keyboard(nullptr), _mouse(nullptr) {
}

InputManager::~InputManager () {
    if (_inputSystem) {
        if (_keyboard) {
            _inputSystem->destroyInputObject(_keyboard);
            _keyboard = nullptr;
        }

        if (_mouse) {
            _inputSystem->destroyInputObject(_mouse);
            _mouse = nullptr;
        }

        OIS::InputManager::destroyInputSystem(_inputSystem);

        _inputSystem = nullptr;

        _keyListeners.clear();
        _mouseListeners.clear();
    }
}

void InputManager::initialise (Ogre::RenderWindow* renderWindow) {
    if (!_inputSystem) {
        OIS::ParamList paramList;
        std::ostringstream windowHndStr;
        size_t windowHnd = 0;
        renderWindow->getCustomAttribute("WINDOW", &windowHnd);
        windowHndStr << windowHnd;
        paramList.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));
        paramList.insert(std::make_pair(std::string("x11_mouse_grab"), std::string("false")));
        //paramList.insert(std::make_pair(std::string("x11_mouse_hide"), std::string("false")));
        paramList.insert(std::make_pair(std::string("x11_keyboard_grab"), std::string("false")));
        paramList.insert(std::make_pair(std::string("XAutoRepeatOn"), std::string("true")));

        _inputSystem = OIS::InputManager::createInputSystem(paramList);

        _keyboard = static_cast<OIS::Keyboard*>(_inputSystem->createInputObject(OIS::OISKeyboard, true));
        _keyboard->setEventCallback(this);

        _mouse = static_cast<OIS::Mouse*>(_inputSystem->createInputObject(OIS::OISMouse, true));
        _mouse->getMouseState().width = renderWindow->getWidth();
        _mouse->getMouseState().height = renderWindow->getHeight();
        _mouse->setEventCallback(this);
    }
}

void InputManager::capture () {
    if (_mouse) {
        _mouse->capture();
    }
    if (_keyboard) {
        _keyboard->capture();
    }
}

void InputManager::addKeyListener(OIS::KeyListener* keyListener, const std::string& instanceName) {
    if (_keyboard) {
        _itKeyListener = _keyListeners.find(instanceName);
        if (_itKeyListener == _keyListeners.end()) {
            _keyListeners[instanceName] = keyListener;
        }
        else {

        }
    }
}

void InputManager::addMouseListener (OIS::MouseListener* mouseListener, const std::string& instanceName) {
    if (_mouse) {
        _itMouseListener = _mouseListeners.find(instanceName);
        if (_itMouseListener == _mouseListeners.end()) {
            _mouseListeners[instanceName] = mouseListener;
        }
        else {

        }
    }
}

void InputManager::removeKeyListener (const std::string& instanceName) {
    _itKeyListener = _keyListeners.find(instanceName);
    if (_itKeyListener == _keyListeners.end()) {
        _keyListeners.erase(_itKeyListener);
    }
    else {

    }
}

void InputManager::removeMouseListener (const std::string& instanceName) {
    _itMouseListener = _mouseListeners.find(instanceName);
    if (_itMouseListener == _mouseListeners.end()) {
        _mouseListeners.erase(_itMouseListener);
    }
    else {

    }
}

void InputManager::removeKeyListener (OIS::KeyListener *keyListener) {
    _itKeyListener = _keyListeners.begin();
    _itKeyListenerEnd = _keyListeners.end();
    for (; _itKeyListener != _itKeyListenerEnd; _itKeyListener++) {
        if (_itKeyListener->second == keyListener) {
            _keyListeners.erase(_itKeyListener);
            break;
        }
    }
}

void InputManager::removeMouseListener (OIS::MouseListener *mouseListener) {
    _itMouseListener = _mouseListeners.begin();
    _itMouseListenerEnd = _mouseListeners.end();
    for (; _itMouseListener != _itMouseListenerEnd; _itMouseListener++) {
        if (_itMouseListener->second == mouseListener) {
            _mouseListeners.erase(_itMouseListener);
            break;
        }
    }
}

OIS::Keyboard* InputManager::getKeyboard () {
    return _keyboard;
}

OIS::Mouse* InputManager::getMouse () {
    return _mouse;
}

InputManager* InputManager::getSingletonPtr () {
    return msSingleton;
}

InputManager& InputManager::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

bool InputManager::keyPressed (const OIS::KeyEvent &e) {
    _itKeyListener = _keyListeners.begin();
    _itKeyListenerEnd = _keyListeners.end();

    for (; _itKeyListener != _itKeyListenerEnd; _itKeyListener++) {
        _itKeyListener->second->keyPressed(e);
    }
    return true;
}

bool InputManager::keyReleased (const OIS::KeyEvent &e) {
    _itKeyListener = _keyListeners.begin();
    _itKeyListenerEnd = _keyListeners.end();

    for (; _itKeyListener != _itKeyListenerEnd; _itKeyListener++) {
        _itKeyListener->second->keyReleased(e);
    }
    return true;
}

bool InputManager::mouseMoved (const OIS::MouseEvent &e) {
    _itMouseListener = _mouseListeners.begin();
    _itMouseListenerEnd = _mouseListeners.end();

    for (; _itMouseListener != _itMouseListenerEnd; _itMouseListener++) {
        _itMouseListener->second->mouseMoved(e);
    }
    return true;
}

bool InputManager::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    _itMouseListener = _mouseListeners.begin();
    _itMouseListenerEnd = _mouseListeners.end();

    for (; _itMouseListener != _itMouseListenerEnd; _itMouseListener++) {
        _itMouseListener->second->mousePressed(e, id);
    }
    return true;
}

bool InputManager::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    _itMouseListener = _mouseListeners.begin();
    _itMouseListenerEnd = _mouseListeners.end();

    for (; _itMouseListener != _itMouseListenerEnd; _itMouseListener++) {
        _itMouseListener->second->mouseReleased(e, id);
    }
    return true;
}

