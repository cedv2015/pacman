#include "InstructionsState.h"

template<> InstructionsState* Ogre::Singleton<InstructionsState>::msSingleton = nullptr;

InstructionsState::InstructionsState () : _root(nullptr),  _sceneMgr(nullptr), _camera(nullptr), _keyboard(nullptr), _mouse(nullptr), _exit(false), _instructions(nullptr), _page(1), _time(0) {
}

InstructionsState* InstructionsState::getSingletonPtr () {
    return msSingleton;
}

InstructionsState& InstructionsState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void InstructionsState::enter () {
	_root = Ogre::Root::getSingletonPtr();
	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("MainCamera");
	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    std::cout << "Entered in InstructionsState" << std::endl;
    wallpaper();
    instructionsShow();
}

void InstructionsState::exit () {
	_sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void InstructionsState::pause () {}

void InstructionsState::resume () {}

bool InstructionsState::frameStarted (const Ogre::FrameEvent &evt) {
    updateMenu(evt.timeSinceLastFrame);
    return !_exit;
}

bool InstructionsState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
}

void InstructionsState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        default:
            break;
    }
}

void InstructionsState::keyReleased (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
            _exit = true;
            break;
        case OIS::KC_P:
            changeState(PlayState::getSingletonPtr());
            break;
        default:
            break;
    }
}

void InstructionsState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}
void InstructionsState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}
void InstructionsState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

CEGUI::MouseButton InstructionsState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}

void InstructionsState::instructionsShow()
{
    if(_instructions == NULL){
        _instructions = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("instructionsWin.layout");
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_instructions);
        CEGUI::Window* frameWindow = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->getChild("Instructions");
        frameWindow->setPosition(CEGUI::UVector2(CEGUI::UDim(0.2,0),CEGUI::UDim(0,0)));
        CEGUI::Window* exitButton = _instructions->getChild("ExitButton");
        exitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&InstructionsState::hide, this));
        CEGUI::Window* leftButton = _instructions->getChild("ButtonLeft");
        leftButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&InstructionsState::previous, this));
        CEGUI::Window* rightButton = _instructions->getChild("ButtonRight");
        rightButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&InstructionsState::next, this));
        for(int i=1; i<=6; i++){
          if(i==_page){
            _instructions->getChild("Image"+std::to_string(i))->show();
            _instructions->getChild("Text"+std::to_string(i))->show();
          }else{
            _instructions->getChild("Image"+std::to_string(i))->hide();
            _instructions->getChild("Text"+std::to_string(i))->hide();
          }
        }

    } else{
        _page = 1;
        for(int i=1; i<=6; i++){
          if(i==_page){
            _instructions->getChild("Image"+std::to_string(i))->show();
            _instructions->getChild("Text"+std::to_string(i))->show();
          }else{
            _instructions->getChild("Image"+std::to_string(i))->hide();
            _instructions->getChild("Text"+std::to_string(i))->hide();
          }
        }
        _instructions->show();
    }

}

bool InstructionsState::hide(const CEGUI::EventArgs &e)
{
    _instructions->hide();
    changeState(MenuState::getSingletonPtr());
    return true;

}

bool InstructionsState::previous(const CEGUI::EventArgs &e)
{
    if(_page>1){
      _page-=1;
      for(int i=1; i<=6; i++){
        if(i==_page){
          _instructions->getChild("Image"+std::to_string(i))->show();
          _instructions->getChild("Text"+std::to_string(i))->show();
        }else{
          _instructions->getChild("Image"+std::to_string(i))->hide();
          _instructions->getChild("Text"+std::to_string(i))->hide();
        }
      }
    }
    return true;

}

bool InstructionsState::next(const CEGUI::EventArgs &e)
{
    if(_page<=5){
      _page+=1;
      for(int i=1; i<=6; i++){
        if(i==_page){
          _instructions->getChild("Image"+std::to_string(i))->show();
          _instructions->getChild("Text"+std::to_string(i))->show();
        }else{
          _instructions->getChild("Image"+std::to_string(i))->hide();
          _instructions->getChild("Text"+std::to_string(i))->hide();
        }
      }
    }
    return true;

}

void InstructionsState::wallpaper(){
    Ogre::TexturePtr m_backgroundTexture = Ogre::TextureManager::getSingleton().createManual("BackgroundTexture",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D,640, 480, 0, Ogre::PF_BYTE_BGR,Ogre::TU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
    Ogre::Image m_backgroundImage;
    m_backgroundImage.load("fondo-piedras-claro.png",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    m_backgroundTexture->loadImage(m_backgroundImage);
    Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("BackgroundMaterial", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    material->getTechnique(0)->getPass(0)->createTextureUnitState("BackgroundTexture");
    material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    material->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(-0.05, 0.0);
    Ogre::Rectangle2D* rect = new Ogre::Rectangle2D(true);
    rect->setCorners(-1.0, 1.0, 1.0, -1.0);
    rect->setMaterial("BackgroundMaterial");
    rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
    rect->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    Ogre::SceneNode* headNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundInstructions");
    headNode->attachObject(rect);

    Ogre::Rectangle2D *rectRails = new Ogre::Rectangle2D(true);
    rectRails->setCorners(-1.0, -0.5, 1.0, -1.0);
    rectRails->setBoundingBox(Ogre::AxisAlignedBox(-100000.0f * Ogre::Vector3::UNIT_SCALE, 100000.0f * Ogre::Vector3::UNIT_SCALE));
    Ogre::MaterialPtr materialRails = Ogre::MaterialManager::getSingleton().getByName("MaterialRails");
    materialRails->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    materialRails->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    materialRails->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    rectRails->setMaterial("MaterialRails");
    materialRails->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(0.15, 0.0);
    rectRails->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
    rectRails->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    Ogre::SceneNode* railsNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundRailsInstructions");
    railsNode->attachObject(rectRails);

    Ogre::Entity *ent_player = _sceneMgr->createEntity("Player_instructions_rails", "Player_rails.mesh");
    Ogre::SceneNode *node_player = _sceneMgr->createSceneNode("Player_rails_instructions_Node");
    _sceneMgr->getRootSceneNode()->addChild(node_player);
    node_player->attachObject(ent_player);
    node_player->setPosition(Ogre::Vector3(3.5,-4,0));
    node_player->scale(Ogre::Vector3(1,1,1));
    node_player->roll(Ogre::Degree(180));
    node_player->pitch(Ogre::Degree(180));
    node_player->yaw(Ogre::Degree(90));
    node_player->translate(Ogre::Vector3(0,1.5,0));
    ent_player->setMaterialName("MaterialPlayer");
}

  void InstructionsState::updateMenu(float delta){
    _time += delta;
    _sceneMgr->getSceneNode("Player_rails_instructions_Node")->setPosition(Ogre::Vector3(3.5,Ogre::Math::Sin(_time*10)*0.05-2.6,0));
}
