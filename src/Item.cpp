#include "Item.h"

int Item::isPlayerColliding (Ogre::Vector2 vec) {
    bool doNotcollidesX = vec.x - _width >= _position.x + _width || vec.x + _width <= _position.x - _width;
    bool doNotcollidesY = vec.y - _width >= _position.y + _width || vec.y + _width <= _position.y - _width;
    if (!doNotcollidesX && !doNotcollidesY) {
        this->take();
        if (_type == "powerUp") {
            return 1;
        }
        else {
            return 2;
        }
    }
    return 0;
}

void Item::update (float delta) {
    _node->yaw(Ogre::Radian(delta));
    if (_taken == true){
        _animation->addTime(delta);
        if (_animation->mComplete) {
            _node->setVisible(false);
            _done = true;
        }
    }
}
