#include "MainState.h"

template<> MainState* Ogre::Singleton<MainState>::msSingleton = nullptr;

MainState::MainState () : _root(nullptr), _sceneMgr(nullptr), _camera(nullptr), _keyboard(nullptr), _mouse(nullptr), _exit(false) {
    _root = Ogre::Root::getSingletonPtr();

    _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
    _sceneMgr->setAmbientLight(Ogre::ColourValue(1,1,1));

    _camera = _sceneMgr->createCamera("MainCamera");
    _reader = new Reader();
    new CameraManager();
}

MainState* MainState::getSingletonPtr () {
    return msSingleton;
}

MainState& MainState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void MainState::enter () {
    std::cout << "Entered in MainState" << std::endl;
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    _camera->setPosition(Ogre::Vector3(0,20,0));
    _camera->lookAt(Ogre::Vector3(0,0,0));
    _camera->setNearClipDistance(0.1);
    _camera->setFarClipDistance(100);
}

void MainState::exit() {
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void MainState::pause () {}

void MainState::resume () {}

bool MainState::frameStarted (const Ogre::FrameEvent &evt) {
    changeState(MenuState::getSingletonPtr());
    return true;
}

bool MainState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
}

void MainState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        default:
            break;
    }
}

void MainState::keyReleased (const OIS::KeyEvent &e) {
    switch (e.key) {
        default:
            break;
    }
}

void MainState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}

void MainState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

void MainState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {}
