#include "MenuState.h"

template<> MenuState* Ogre::Singleton<MenuState>::msSingleton = nullptr;

MenuState::MenuState () :  _root(nullptr), _sceneMgr(nullptr), _camera(nullptr), _keyboard(nullptr), _mouse(nullptr), _exit(false), _sheet(nullptr), _instructions(nullptr), _time(0) {

}

MenuState* MenuState::getSingletonPtr () {
    return msSingleton;
}

MenuState& MenuState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void MenuState::enter () {
    std::cout << "Entered in MenuState" << std::endl;
    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    menu();
    createGUI();
}

void MenuState::exit () {
    _sheet->hide();
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void MenuState::pause () {}

void MenuState::resume () {}

bool MenuState::frameStarted (const Ogre::FrameEvent &evt) {
    updateMenu(evt.timeSinceLastFrame);
    return !_exit;
}

bool MenuState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
}

void MenuState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        default:
            break;
    }
}

void MenuState::keyReleased (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
            _exit = true;
            break;
        case OIS::KC_P:
            changeState(PlayState::getSingletonPtr());
            break;
        default:
            break;
    }
}

void MenuState::mouseMoved(const OIS::MouseEvent& e)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}

void MenuState::mousePressed(const OIS::MouseEvent& e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void MenuState::mouseReleased(const OIS::MouseEvent& e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

CEGUI::MouseButton MenuState::convertMouseButton(OIS::MouseButtonID id)
{
    CEGUI::MouseButton ceguiId;
    switch(id)
    {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }
    return ceguiId;
}

void MenuState::createGUI()
{
    if(_sheet == NULL){

        _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Game/SheetMenu");

        //Start button
        CEGUI::Window* playButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","Game/PlayButton");
        playButton->setText("Play");
        playButton->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.08,0)));
        playButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4,0),CEGUI::UDim(0.15,0)));
        playButton->subscribeEvent(CEGUI::PushButton::EventClicked,
        CEGUI::Event::Subscriber(&MenuState::play, this));

        //Instructions button
        CEGUI::Window* instructionsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","Game/InstructionsButton");
        instructionsButton->setText("Instructions");
        instructionsButton->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.08,0)));
        instructionsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4,0),CEGUI::UDim(0.25,0)));
        instructionsButton->subscribeEvent(CEGUI::PushButton::EventClicked,
        CEGUI::Event::Subscriber(&MenuState::instructions, this));

        //Ranking button
        CEGUI::Window* rankingButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","Game/RankingButton");
        rankingButton->setText("Ranking");
        rankingButton->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.08,0)));
        rankingButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4,0),CEGUI::UDim(0.35,0)));
        rankingButton->subscribeEvent(CEGUI::PushButton::EventClicked,
        CEGUI::Event::Subscriber(&MenuState::ranking, this));

        //Quit button
        CEGUI::Window* quitButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","Game/QuitButton");
        quitButton->setText("Exit");
        quitButton->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.08,0)));
        quitButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4,0),CEGUI::UDim(0.45,0)));
        quitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
        CEGUI::Event::Subscriber(&MenuState::quit, this));

        //Attaching buttons
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(playButton);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(rankingButton);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(instructionsButton);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(quitButton);
       _sheet->addChild(playButton);

        _sheet->addChild(rankingButton);
        _sheet->addChild(instructionsButton);
        _sheet->addChild(quitButton);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_sheet);

    }else{
        _sheet->show();
    }

}

bool MenuState::play(const CEGUI::EventArgs &e)
{
    changeState(PlayState::getSingletonPtr());

    return true;
}

bool MenuState::quit(const CEGUI::EventArgs &e)
{
    _exit = true;
    return true;
}

bool MenuState::instructions(const CEGUI::EventArgs &e)
{
    changeState(InstructionsState::getSingletonPtr());
    return true;
}

bool MenuState::ranking(const CEGUI::EventArgs &e)
{
    changeState(RankingState::getSingletonPtr());
    return true;
}

bool MenuState::hide(const CEGUI::EventArgs &e)
{
    _instructions->hide();
    return true;

}

void MenuState::menu(){

    _sceneMgr->getCamera("MainCamera")->setPosition(Ogre::Vector3(0, 0, 10));
    _sceneMgr->getCamera("MainCamera")->lookAt(Ogre::Vector3(0, 0, 8));

    Ogre::TexturePtr m_backgroundTexture = Ogre::TextureManager::getSingleton().createManual("BackgroundTexture",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D,640, 480, 0, Ogre::PF_BYTE_BGR,Ogre::TU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
    Ogre::Image m_backgroundImage;
    m_backgroundImage.load("fondo-piedras-claro.png",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    m_backgroundTexture->loadImage(m_backgroundImage);
    Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("BackgroundMaterial", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    material->getTechnique(0)->getPass(0)->createTextureUnitState("BackgroundTexture");
    material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    material->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(0.05, 0.0);
    Ogre::Rectangle2D* rect = new Ogre::Rectangle2D(true);
    rect->setCorners(-1.0, 1.0, 1.0, -1.0);
    rect->setMaterial("BackgroundMaterial");
    rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
    rect->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    Ogre::SceneNode* headNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundMenu");
    headNode->attachObject(rect);

    /*Ogre::Entity *ent_brown = _sceneMgr->createEntity("Brown", "Player_rails.mesh");
    Ogre::SceneNode *node_browner = _sceneMgr->createSceneNode("Brown_Node");
    _sceneMgr->getRootSceneNode()->addChild(node_browner);
    node_browner->attachObject(ent_brown);
    node_browner->setPosition(Ogre::Vector3(0,0,0));
    node_browner->scale(Ogre::Vector3(1,1.8,1.25));
    node_browner->roll(Ogre::Degree(180));
    node_browner->pitch(Ogre::Degree(180));
    node_browner->yaw(Ogre::Degree(90));
    node_browner->translate(Ogre::Vector3(0,1.15,0));
    ent_brown->setMaterialName("MaterialBrown");*/

    Ogre::Rectangle2D *rectRails = new Ogre::Rectangle2D(true);
    rectRails->setCorners(-1.0, -0.5, 1.0, -1.0);
    rectRails->setBoundingBox(Ogre::AxisAlignedBox(-100000.0f * Ogre::Vector3::UNIT_SCALE, 100000.0f * Ogre::Vector3::UNIT_SCALE));
    Ogre::MaterialPtr materialRails = Ogre::MaterialManager::getSingleton().getByName("MaterialRails");
    materialRails->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    materialRails->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    materialRails->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    rectRails->setMaterial("MaterialRails");
    materialRails->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(0.15, 0.0);
    rectRails->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
    rectRails->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    Ogre::SceneNode* railsNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundRails");
    railsNode->attachObject(rectRails);

    Ogre::Entity *ent_player = _sceneMgr->createEntity("Player_rails", "Player_rails.mesh");
    Ogre::SceneNode *node_player = _sceneMgr->createSceneNode("Player_rails_Node");
    _sceneMgr->getRootSceneNode()->addChild(node_player);
    node_player->attachObject(ent_player);
    node_player->setPosition(Ogre::Vector3(3.5,-4,0));
    node_player->scale(Ogre::Vector3(1,1,1));
    node_player->roll(Ogre::Degree(180));
    node_player->pitch(Ogre::Degree(180));
    node_player->yaw(Ogre::Degree(90));
    node_player->translate(Ogre::Vector3(0,1.5,0));
    ent_player->setMaterialName("MaterialPlayer");

    Ogre::Entity *ent_enemy1 = _sceneMgr->createEntity("Enemy1", "Player_rails.mesh");
    Ogre::SceneNode *node_enemy1 = _sceneMgr->createSceneNode("Enemy1_Node");
    _sceneMgr->getRootSceneNode()->addChild(node_enemy1);
    node_enemy1->attachObject(ent_enemy1);
    node_enemy1->scale(Ogre::Vector3(0.75,0.75,0.75));
    node_enemy1->roll(Ogre::Degree(180));
    node_enemy1->pitch(Ogre::Degree(180));
    node_enemy1->yaw(Ogre::Degree(90));
    ent_enemy1->setMaterialName("MaterialEnemy");

    Ogre::Entity *ent_enemy2 = _sceneMgr->createEntity("Enemy2", "Player_rails.mesh");
    Ogre::SceneNode *node_enemy2 = _sceneMgr->createSceneNode("Enemy2_Node");
    _sceneMgr->getRootSceneNode()->addChild(node_enemy2);
    node_enemy2->attachObject(ent_enemy2);
    node_enemy2->scale(Ogre::Vector3(0.6,0.6,0.6));
    node_enemy2->roll(Ogre::Degree(180));
    node_enemy2->pitch(Ogre::Degree(180));
    node_enemy2->yaw(Ogre::Degree(90));
    ent_enemy2->setMaterialName("MaterialEnemy");

    Ogre::Entity *ent_coin1 = _sceneMgr->createEntity("Coin1", "Player_rails.mesh");
    Ogre::SceneNode *node_coin1 = _sceneMgr->createSceneNode("Coin1_Node");
    _sceneMgr->getRootSceneNode()->addChild(node_coin1);
    node_coin1->attachObject(ent_coin1);
    node_coin1->scale(Ogre::Vector3(0.5,0.5,0.5));
    node_coin1->roll(Ogre::Degree(180));
    node_coin1->pitch(Ogre::Degree(180));
    node_coin1->yaw(Ogre::Degree(90));
    ent_coin1->setMaterialName("MaterialCoin");

    Ogre::Entity *ent_coin2 = _sceneMgr->createEntity("Coin2", "Player_rails.mesh");
    Ogre::SceneNode *node_coin2 = _sceneMgr->createSceneNode("Coin2_Node");
    _sceneMgr->getRootSceneNode()->addChild(node_coin2);
    node_coin2->attachObject(ent_coin2);
    node_coin2->scale(Ogre::Vector3(0.5,0.5,0.5));
    node_coin2->roll(Ogre::Degree(180));
    node_coin2->pitch(Ogre::Degree(180));
    node_coin2->yaw(Ogre::Degree(90));
    ent_coin2->setMaterialName("MaterialCoin");

    Ogre::Entity *ent_coin3 = _sceneMgr->createEntity("Coin3", "Player_rails.mesh");
    Ogre::SceneNode *node_coin3 = _sceneMgr->createSceneNode("Coin3_Node");
    _sceneMgr->getRootSceneNode()->addChild(node_coin3);
    node_coin3->attachObject(ent_coin3);
    node_coin3->scale(Ogre::Vector3(0.5,0.5,0.5));
    node_coin3->roll(Ogre::Degree(180));
    node_coin3->pitch(Ogre::Degree(180));
    node_coin3->yaw(Ogre::Degree(90));
    ent_coin3->setMaterialName("MaterialCoin");

    Ogre::Entity *ent_coin4 = _sceneMgr->createEntity("Coin4", "Player_rails.mesh");
    Ogre::SceneNode *node_coin4 = _sceneMgr->createSceneNode("Coin4_Node");
    _sceneMgr->getRootSceneNode()->addChild(node_coin4);
    node_coin4->attachObject(ent_coin4);
    node_coin4->scale(Ogre::Vector3(0.5,0.5,0.5));
    node_coin4->roll(Ogre::Degree(180));
    node_coin4->pitch(Ogre::Degree(180));
    node_coin4->yaw(Ogre::Degree(90));
    ent_coin4->setMaterialName("MaterialCoin");

    Ogre::Entity *ent_coin5 = _sceneMgr->createEntity("Coin5", "Player_rails.mesh");
    Ogre::SceneNode *node_coin5 = _sceneMgr->createSceneNode("Coin5_Node");
    _sceneMgr->getRootSceneNode()->addChild(node_coin5);
    node_coin5->attachObject(ent_coin5);
    node_coin5->scale(Ogre::Vector3(0.5,0.5,0.5));
    node_coin5->roll(Ogre::Degree(180));
    node_coin5->pitch(Ogre::Degree(180));
    node_coin5->yaw(Ogre::Degree(90));
    ent_coin5->setMaterialName("MaterialCoin");
}

void MenuState::updateMenu(float delta){
    _time += delta;
    _sceneMgr->getSceneNode("Player_rails_Node")->setPosition(Ogre::Vector3(3.5,Ogre::Math::Sin(_time*10)*0.05-2.6,0));
    _sceneMgr->getSceneNode("Enemy1_Node")->setPosition(Ogre::Vector3(-2.5,Ogre::Math::Sin(_time*7)*0.05,0));
    _sceneMgr->getSceneNode("Enemy1_Node")->pitch(Ogre::Degree(Ogre::Math::Sin(_time*7)*0.005));
    _sceneMgr->getSceneNode("Enemy2_Node")->setPosition(Ogre::Vector3(2.5,Ogre::Math::Sin(_time*7)*-0.05+2.5,0));
    _sceneMgr->getSceneNode("Enemy2_Node")->pitch(Ogre::Degree(Ogre::Math::Sin(_time*10)*0.007));
    _sceneMgr->getSceneNode("Coin1_Node")->setPosition(Ogre::Vector3(-4,Ogre::Math::Sin(_time*5)*-0.5-2,0));
    _sceneMgr->getSceneNode("Coin1_Node")->pitch(Ogre::Degree(Ogre::Math::Sin(_time*10)*0.007));
    _sceneMgr->getSceneNode("Coin2_Node")->setPosition(Ogre::Vector3(-2.5,Ogre::Math::Sin(_time*5)*0.5-2,0));
    _sceneMgr->getSceneNode("Coin2_Node")->pitch(Ogre::Degree(Ogre::Math::Sin(_time*10)*0.007));
    _sceneMgr->getSceneNode("Coin3_Node")->setPosition(Ogre::Vector3(-1,Ogre::Math::Sin(_time*5)*-0.5-2,0));
    _sceneMgr->getSceneNode("Coin3_Node")->pitch(Ogre::Degree(Ogre::Math::Sin(_time*10)*0.007));
    _sceneMgr->getSceneNode("Coin4_Node")->setPosition(Ogre::Vector3(0.5,Ogre::Math::Sin(_time*5)*0.5-2,0));
    _sceneMgr->getSceneNode("Coin4_Node")->pitch(Ogre::Degree(Ogre::Math::Sin(_time*10)*0.007));
    _sceneMgr->getSceneNode("Coin5_Node")->setPosition(Ogre::Vector3(2,Ogre::Math::Sin(_time*5)*-0.5-2,0));
    _sceneMgr->getSceneNode("Coin5_Node")->pitch(Ogre::Degree(Ogre::Math::Sin(_time*10)*0.007));
}
