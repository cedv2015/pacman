#include "Node.h"

void Node::addEdge (int cost, int position, Node* n) {
    switch (position) {
        case 0:
            _up = std::make_pair(n, cost);
            break;
        case 1:
            _right = std::make_pair(n, cost);
            break;
        case 2:
            _down = std::make_pair(n, cost);
            break;
        case 3:
            _left = std::make_pair(n, cost);
            break;
        default:
            break;
    }
}

Node* Node::selectRandomAdyacent () {
    Node *n = nullptr;
    while (n == nullptr) {
        int r = rand() % 4;
        switch (r) {
            case 0:
                n = _up.first;
                break;
            case 1:
                n = _right.first;
                break;
            case 2:
                n = _down.first;
                break;
            case 3:
                n = _left.first;
                break;
            default:
                break;
        }
    }
    return n;
}

bool Node::isEdge () {
    if (_up.first == nullptr && _down.first == nullptr && _left.first != nullptr && _right.first != nullptr) {
        if (_left.second != 0 && _right.second != 0){
            _left.first->replaceEdge(_left.second + _right.second, 1, _right.first);
            _right.first->replaceEdge(_right.second + _left.second, 3, _left.first);
            return true;
        }
    }
    else if (_up.first != nullptr && _down.first != nullptr && _left.first == nullptr && _right.first == nullptr){
        if (_down.second != 0 && _up.second != 0) {
            _up.first->replaceEdge(_down.second + _up.second, 2, _down.first);
            _down.first->replaceEdge(_down.second + _up.second, 0, _up.first);
            return true;
        }
    }
    return false;
}

void Node::replaceEdge (int cost, int position, Node* n) {
    switch (position) {
        case 0:
            _up = std::make_pair(n, cost);
            break;
        case 1:
            _right = std::make_pair(n, cost);
            break;
        case 2:
            _down = std::make_pair(n, cost);
            break;
        case 3:
            _left = std::make_pair(n, cost);
            break;
        default:
            break;
    }   
}

int Node::getCost (Node *node) {
    int cost = 100;
    if (_up.first == node) {
        cost = _up.second;
    }
    else if (_down.first == node) {
        cost = _down.second;
    }
    else if (_left.first == node) {
        cost = _left.second;
    }
    else if (_right.first == node) {
        cost = _right.second;
    }
    return cost;
}

std::vector<Node*> Node::getAdyacents () {
    std::vector<Node*> v;
    if (_up.first) {
        v.push_back(_up.first);
    }
    if (_down.first) {
        v.push_back(_down.first);
    }
    if (_left.first) {
        v.push_back(_left.first);
    }
    if (_right.first) {
        v.push_back(_right.first);
    }
    return v;
}
