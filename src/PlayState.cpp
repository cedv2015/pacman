#include "PlayState.h"
#include "Reader.h"
#include "MenuState.h"
#include <math.h> 
#include "SoundFXManager.h"

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = nullptr;

PlayState::PlayState () :_root(nullptr), _sceneMgr(nullptr), _camera(nullptr), _keyboard(nullptr), _mouse(nullptr), _exit(false), _rankingMgr(nullptr), _overlay(nullptr), _gameOver(nullptr) {

}

PlayState* PlayState::getSingletonPtr () {
    return msSingleton;
}

PlayState& PlayState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void PlayState::enter () {
    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    
    Reader* pReader = Reader::getSingletonPtr();
    pReader->Reader::resetLevel();
    pReader->Reader::convertMatrix(std::to_string(pReader->getLevel()));
    _player = pReader->getPlayer();
    _player->restartLifes();
    _enemies = pReader->getEnemies();

    _rankingMgr = RankingManager::getSingletonPtr();
    _rankingMgr->getRanking();
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide( );
    showOverlay();
    reset();
}

void PlayState::exit () {
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show( );
    _player->resetTime();
    _player->resetScore();
    _overlay->hide();
    if (_gameOver) {
        delete _gameOver;
    }
}

void PlayState::pause () {

}

void PlayState::resume () {

}

bool PlayState::frameStarted (const Ogre::FrameEvent &evt) {
    //  Update all dynamic entities
    bool game_ended = _player->isDead() || Reader::getSingletonPtr()->isGameOver();
    if (game_ended) {
        if (_gameOver == nullptr) {
            _gameOver = new GameOver();
        }
        else {
            if (_gameOver->update(evt.timeSinceLastFrame)) {
                if(_rankingMgr->checkRanking(_player->getScore())){
                    AddRankingState::getSingletonPtr()->setScore(_player->getScore());
                    std::cout << "add"<< std::endl;
                    this->changeState (AddRankingState::getSingletonPtr());
                } else {
                    this->changeState (MenuState::getSingletonPtr());
                    std::cout << "menu"<< std::endl;
                }
            }
        }
    }
    else {
        _player->update(evt.timeSinceLastFrame);
        lifes();
    }
    return !_exit;
}

bool PlayState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
}

void PlayState::keyPressed (const OIS::KeyEvent &e) {
    /*
        Check if arrows are pressed and call functions from player
        Check if pause button is pressed and pause the game
    */
    switch (e.key) {
        case OIS::KC_UP:
            _player->pressUp();
            break;
        case OIS::KC_DOWN:
            _player->pressDown();
            break;
        case OIS::KC_LEFT:
            _player->pressLeft();
            break;
        case OIS::KC_RIGHT:
            _player->pressRight();
            break;
        default:
            break;
    }
}

void PlayState::keyReleased (const OIS::KeyEvent &e) {
    /*
        Check if arrows are released and call functions from player
    */
    switch (e.key) {
        case OIS::KC_UP:
            _player->releaseUp();
            break;
        case OIS::KC_DOWN:
            _player->releaseDown();
            break;
        case OIS::KC_LEFT:
            _player->releaseLeft();
            break;
        case OIS::KC_RIGHT:
            _player->releaseRight();
            break;
        case OIS::KC_ESCAPE:
            _exit = true;
            break;
        case OIS::KC_SPACE:
            this->changeState (MenuState::getSingletonPtr());
            break;
        case OIS::KC_Q:
            Reader::getSingletonPtr()->loadNextStage();
            break;
        case OIS::KC_P:
            SoundFXManager::getSingletonPtr()->load("pause.wav")->play();
            this->pushState (PauseState::getSingletonPtr());
        default:
            break;
    }
}

    //  No mouse controls

void PlayState::mouseMoved (const OIS::MouseEvent &e) {}

void PlayState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

void PlayState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

void PlayState::showOverlay(){

    if(_overlay == NULL){
        _overlay = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Game/Overlay");
        CEGUI::Window* overlay = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Overlay","Game/Panel");
        overlay->setSize(CEGUI::USize(CEGUI::UDim(0.6,0),CEGUI::UDim(0.15,0)));
        overlay->setPosition(CEGUI::UVector2(CEGUI::UDim(0.2,0),CEGUI::UDim(0.0,0)));
        _lifes = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Game/Lifes");
        CEGUI::Window* life1 = _lifes->createChild("TaharezLook/Life","Life1");
        life1->setSize(CEGUI::USize(CEGUI::UDim(0.03,0),CEGUI::UDim(0.03,0)));
        life1->setPosition(CEGUI::UVector2(CEGUI::UDim(0.25,0),CEGUI::UDim(0.055,0)));
        CEGUI::Window* life2 = _lifes->createChild("TaharezLook/Life","Life2");
        life2->setSize(CEGUI::USize(CEGUI::UDim(0.03,0),CEGUI::UDim(0.03,0)));
        life2->setPosition(CEGUI::UVector2(CEGUI::UDim(0.29,0),CEGUI::UDim(0.055,0)));
        CEGUI::Window* life3 = _lifes->createChild("TaharezLook/Life","Life3");
        life3->setSize(CEGUI::USize(CEGUI::UDim(0.03,0),CEGUI::UDim(0.03,0)));
        life3->setPosition(CEGUI::UVector2(CEGUI::UDim(0.33,0),CEGUI::UDim(0.055,0)));

        CEGUI::Window* clock = _lifes->createChild("TaharezLook/Time","Clock");
        clock->setSize(CEGUI::USize(CEGUI::UDim(0.05,0),CEGUI::UDim(0.05,0)));
        clock->setPosition(CEGUI::UVector2(CEGUI::UDim(0.67,0),CEGUI::UDim(0.05,0)));
        CEGUI::Window* time = _lifes->createChild("TaharezLook/Label","Time");
        time->setPosition(CEGUI::UVector2(CEGUI::UDim(0.69,0),CEGUI::UDim(0.055,0)));
        time->setText(std::to_string(_player->getTime()));

        CEGUI::Window* coin = _lifes->createChild("TaharezLook/Score","Coin");
        coin->setSize(CEGUI::USize(CEGUI::UDim(0.05,0),CEGUI::UDim(0.05,0)));
        coin->setPosition(CEGUI::UVector2(CEGUI::UDim(0.47,0),CEGUI::UDim(0.05,0)));
        CEGUI::Window* score = _lifes->createChild("TaharezLook/Label","Score");
        score->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5,0),CEGUI::UDim(0.055,0)));
        score->setText(std::to_string(_player->getScore()));

        CEGUI::Window* stage = _lifes->createChild("TaharezLook/Label","Stage");
        stage->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0,0),CEGUI::UDim(0.055,0)));
        stage->setText("Stage");

        CEGUI::Window* stageNum = _lifes->createChild("TaharezLook/Label","StageNum");
        stageNum->setPosition(CEGUI::UVector2(CEGUI::UDim(0.05,0),CEGUI::UDim(0.055,0)));
        stageNum->setText(std::to_string(Reader::getSingletonPtr()->getLevel()));

        _overlay->addChild(overlay);
        _overlay->addChild(_lifes);
        _overlay->addChild(time);
        _overlay->addChild(score);
        _overlay->addChild(stage);
        _overlay->addChild(stageNum);
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_overlay);
    }else{
        _overlay->show();
    }
}

void PlayState::lifes(){
    int life = _player->getLife();
    if (life < 0) return;
    for(int i=life; i<3; i++){
        _lifes->getChild("Life"+std::to_string(i+1))->hide();
    }
    _overlay->getChild("Time")->setText(std::to_string((int)_player->getTime()));
    _overlay->getChild("Score")->setText(std::to_string((int)_player->getScore()));
    _overlay->getChild("StageNum")->setText(std::to_string(Reader::getSingletonPtr()->getLevel()));
}

void PlayState::reset(){
    //reset lifes images
    for(int i=1; i<=3; i++){
        _lifes->getChild("Life"+std::to_string(i))->show();
    }
    //reset score
    _player->resetScore();
}
