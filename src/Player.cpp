#include "Player.h"
#include "RankingManager.h"
#include "CameraManager.h"

Player::Player (float speed, Ogre::Vector2 position, Ogre::SceneNode *node, Ogre::Entity *entity, Graph *graph, float stage_width) : DynamicEntity(speed, position, node, entity, graph), _state(Iddle), _life(3), _score(0), _timer(0), _time_limit(20), _up(false), _down(false), _left(false), _right(false), _stage_width(stage_width), _time_attack(5) {
    setAnimation ("Iddle", 0.367);
    _graph->setPlayerPosition(std::to_string(int(-_position.x)) + ":" + std::to_string(int(-_position.y))); 
    _coinSound = SoundFXManager::getSingletonPtr()->load("Pickup_Coin.wav");
    _powerUp = SoundFXManager::getSingletonPtr()->load("power_up.wav");
    _dead = SoundFXManager::getSingletonPtr()->load("dead.wav");
};

void Player::update (float delta) {
    if (_graph->update(delta)) {
        _timer += delta;
        _time_attack += delta;
        Ogre::Vector2 distance(0,0);
        if (_up) {
            distance.y += _speed * delta;
        }
        if (_down) {
            distance.y -= _speed * delta;
        }
        _position.y += distance.y;
        if (this->checkCollision() == true) {
            _position.y -= distance.y;
            distance.y = 0.0;
        }
        if (_left) {
            distance.x -= _speed * delta;
        }
        if (_right) {
            distance.x += _speed * delta;
        }
        _position.x += distance.x;
        if (this->checkCollision() == true) {
            _position.x -= distance.x;
            distance.x = 0.0;
        }
        _node->translate(Ogre::Vector3((distance.x),0,-distance.y));
        checkTeleport ();
        if (distance.x != 0) {
            if (distance.x > 0) {
                _direction = 3;
            }
            else {
                _direction = 1;
            }
        } else if(distance.y != 0) {
            if (distance.y > 0) {
                _direction = 0;
            }
            else {
                _direction = 2;
            }
        }
        setDirection (_direction);

        if (_state == Attack) {
            if (_time_attack > 5) {
                _state = Run;
                setAnimation("Run", 2.867);
                _graph->setEnemiesNormal();
            }
        }
        else if (distance.x == 0 && distance.y == 0) {
            if (_state != Iddle) {
                _state = Iddle;
                setAnimation ("Iddle", 0.367);
            }
        }
        else {
            if (_state != Run) {
                _state = Run;
                setAnimation ("Run", 2.867);
            }
        }

        if (this->checkEnemies ()) {
            if (_state == Attack) {
                _score += 100;
            }
            else{
                _life--;
                if (_life < 0) {
                    this->resetTime();
                }
                else {
                    this->restartPosition ();
                    _graph->restartEnemies ();
                    this->resetTime();
                    int x = -_position.x;
                    int y = -_position.y;
                    _graph->setPlayerPosition(std::to_string(x) + ":" + std::to_string(y));
                }
            }
        }
        _animation->addTime(delta);
        this->checkItems ();
        if (std::abs(std::remainder(_position.x, 1.0f)) < 0.1 && std::abs(std::remainder(_position.y, 1.0f)) < 0.1) {
            std::string position = std::to_string(int(-_position.x + 0.5)) + ":" + std::to_string(int(-_position.y + 0.5));
            _graph->setPlayerPosition(position);
        }
    }
}

void Player::nextStage (int x, int y, Graph *graph, Ogre::SceneNode *node, Ogre::Entity *ent, float time_limit, int stage_width) {
    _position.x = _init_position.x = x;
    _position.y = _init_position.y = y;
    _graph = graph;
    _node = node;
    _entity = ent;
    this->addTimeScore ();
    _time_limit = time_limit;
    _stage_width = stage_width;
    _state = Iddle;
    delete _animation;
    _animation = new AnimationBlender(ent);
    setAnimation("Iddle", 0.367);
    _graph->setPlayerPosition (std::to_string(-x) + ":" + std::to_string(-y));
}

bool Player::checkCollision () {
    return _graph->isPlayerCollidingWithWalls (_position);
}

void Player::addTimeScore () {
    if (_timer > _time_limit) {
        return;
    }
    int points = _time_limit - _timer;
    _score += points;
    _timer = 0;
}


bool Player::checkEnemies () {
    bool dead = _graph->isPlayerCollidingWithEnemies (_position, _state);
    if (dead) {
        _dead->play();
    }

    return dead;
}

void Player::checkItems () {
    int type = _graph->isPlayerCollidingWithItems (_position);
    if (type == 1) {
        //TODO: Immunity
        _state = Attack;
        setAnimation ("Attack", 1);
        _time_attack = 0;
        CameraManager::getSingletonPtr()->attack();
        _powerUp->play();
    }
    else if (type == 2) {
        _coinSound->play();
        _score += 10;
    }
}

void Player::checkTeleport () {
    if (_position.x < -_stage_width + 1) {
        _position.x = 0;
        _node->setPosition (Ogre::Vector3 (_position.x, 0, - _position.y));
    }
    else if (_position.x > 0) {
        _position.x = -_stage_width + 1;
        _node->setPosition (Ogre::Vector3 (_position.x, 0, - _position.y));
    }
}

void Player::setAnimation (const Ogre::String &animation, Ogre::Real duration) {
    _animation->blend(animation, AnimationBlender::Switch, duration, true);
}
