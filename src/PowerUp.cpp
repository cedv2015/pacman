#include "PowerUp.h"

void PowerUp::take () {
    _taken = true;
    _animation->blend("Dissapear", AnimationBlender::Blend, 0.5, false);
    return;
}
