#include "RankingManager.h"

template <> RankingManager* Ogre::Singleton<RankingManager>::msSingleton = nullptr;

RankingManager* RankingManager::getSingletonPtr () {
    return msSingleton;
}

RankingManager& RankingManager::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

std::vector<std::string> RankingManager::getRanking () {
// checks if the vector contains something, if not it calls "loadRankings"
    if (_rankings.size() == 0) {
        this->loadRankings();
    }
    return _rankings;
}

bool RankingManager::checkRanking (int score) {
// checks if the score is better than the actual 10th position
    if (score > std::stoi(_rankings[19])) {
        return true;
    }
    return false;
}

void RankingManager::setRanking (std::string name, int score) {
// changes the position one by one from the last to the position of the actual score
    if (_rankings.size() == 0) {
        this->loadRankings();
    }
    if (!this->checkRanking(score)) {
        return;
    }
    int position = 11;
    int i;
    for (i = 10; i > 0; i--) {
        if (score < stoi(_rankings[i * 2 - 1])) {
            break;
        }
        position--;
    }
    for (i = 10; i > position; i--) {
        _rankings[i * 2 - 1] = _rankings[i * 2 - 3];
        _rankings[i * 2 - 2] = _rankings[i * 2 - 4];
    }
    _rankings[position * 2 - 2] = name;
    _rankings[position * 2 - 1] = std::to_string(score);
    this->saveRankings ();
    std::cout << "Ranking saved" << std::endl;
}

void RankingManager::saveRankings () {
// evens contain the name of the player
// odds contain the number of score
    std::ofstream file ("ranking.txt");
    if (file.is_open()) {
        for (int i = 0; i<20; i++) {
            file << _rankings[i] << std::endl;
        }
        file.close();
    }
}

void RankingManager::loadRankings () {
// evens contain the name of the player
// odds contain the number of score
    std::string line;
    std::ifstream file ("ranking.txt");
    if (file.is_open()) {
        while (getline(file, line)) {
            _rankings.push_back(line);
        }
        file.close();
    }
}
