#include "RankingState.h"

template<> RankingState* Ogre::Singleton<RankingState>::msSingleton = nullptr;

RankingState::RankingState () :_root(nullptr),  _sceneMgr(nullptr), _camera(nullptr), _rankingManager(nullptr), _keyboard(nullptr), _mouse(nullptr), _exit(false), _records(nullptr), _time(0) {

	_rankingManager = RankingManager::getSingletonPtr();

}

RankingState* RankingState::getSingletonPtr () {
    return msSingleton;
}

RankingState& RankingState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void RankingState::enter () {
	_root = Ogre::Root::getSingletonPtr();
	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("MainCamera");
	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    std::cout << "Entered in RankingState" << std::endl;
    wallpaper();
    rankingShow();
}

void RankingState::exit () {
	_sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void RankingState::pause () {}

void RankingState::resume () {}

bool RankingState::frameStarted (const Ogre::FrameEvent &evt) {
    updateMenu(evt.timeSinceLastFrame);
    return !_exit;
}

bool RankingState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
}

void RankingState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        default:
            break;
    }
}

void RankingState::keyReleased (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
            _exit = true;
            break;
        case OIS::KC_P:
            changeState(PlayState::getSingletonPtr());
            break;
        default:
            break;
    }
}

void RankingState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}
void RankingState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}
void RankingState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

CEGUI::MouseButton RankingState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}

void RankingState::rankingShow()
{
    if(_records == NULL){
        _records = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("rankingWin.layout");
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_records);
        CEGUI::Window* frameWindow = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->getChild("Ranking");
        frameWindow->setPosition(CEGUI::UVector2(CEGUI::UDim(0.25,0),CEGUI::UDim(0.1,0)));
        CEGUI::Window* exitButton = _records->getChild("ExitButton");
        exitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&RankingState::hide, this));

	    std::vector<std::string> ranking = _rankingManager->getRanking();
        std::string name, score;
        for (unsigned int i=0; i<ranking.size(); i+=2){
            name = std::string("Name") + std::to_string(i/2+1) + std::string("Text");
            score = std::string("Score") + std::to_string(i/2+1) + std::string("Text");
            CEGUI::Window* name_record = _records->getChild(name);
            name_record->setText(ranking[i]);
            CEGUI::Window* score_record = _records->getChild(score);
            score_record ->setText(ranking[i+1]);
        }

    }else{
        _records->show();
	    std::vector<std::string> ranking = _rankingManager->getRanking();
        std::string name, score;
        for (unsigned int i = 0; i < ranking.size(); i += 2) {
            name = std::string("Name") + std::to_string(i / 2 + 1) + std::string("Text");
            score = std::string("Score") + std::to_string(i / 2 + 1) + std::string("Text");
            CEGUI::Window* name_record = _records->getChild(name);
            name_record->setText(ranking[i]);
            CEGUI::Window* score_record = _records->getChild(score);
            score_record ->setText(ranking[i+1]);
        }
    }

}

bool RankingState::hide(const CEGUI::EventArgs &e)
{
    _records->hide();
    changeState(MenuState::getSingletonPtr());
    return true;

}

void RankingState::wallpaper(){
    Ogre::TexturePtr m_backgroundTexture = Ogre::TextureManager::getSingleton().createManual("BackgroundTexture",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D,640, 480, 0, Ogre::PF_BYTE_BGR,Ogre::TU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
    Ogre::Image m_backgroundImage;
    m_backgroundImage.load("fondo-piedras-claro.png",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    m_backgroundTexture->loadImage(m_backgroundImage);
    Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("BackgroundMaterial", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    material->getTechnique(0)->getPass(0)->createTextureUnitState("BackgroundTexture");
    material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    material->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(-0.05, 0.0);
    Ogre::Rectangle2D* rect = new Ogre::Rectangle2D(true);
    rect->setCorners(-1.0, 1.0, 1.0, -1.0);
    rect->setMaterial("BackgroundMaterial");
    rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
    rect->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    Ogre::SceneNode* headNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("Backgroundranking");
    headNode->attachObject(rect);

    Ogre::Rectangle2D *rectRails = new Ogre::Rectangle2D(true);
    rectRails->setCorners(-1.0, -0.5, 1.0, -1.0);
    rectRails->setBoundingBox(Ogre::AxisAlignedBox(-100000.0f * Ogre::Vector3::UNIT_SCALE, 100000.0f * Ogre::Vector3::UNIT_SCALE));
    Ogre::MaterialPtr materialRails = Ogre::MaterialManager::getSingleton().getByName("MaterialRails");
    materialRails->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    materialRails->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    materialRails->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    rectRails->setMaterial("MaterialRails");
    materialRails->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(0.15, 0.0);
    rectRails->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
    rectRails->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    Ogre::SceneNode* railsNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundRailsranking");
    railsNode->attachObject(rectRails);

    Ogre::Entity *ent_player = _sceneMgr->createEntity("Player_ranking_rails", "Player_rails.mesh");
    Ogre::SceneNode *node_player = _sceneMgr->createSceneNode("Player_rails_ranking_Node");
    _sceneMgr->getRootSceneNode()->addChild(node_player);
    node_player->attachObject(ent_player);
    node_player->setPosition(Ogre::Vector3(3.5,-4,0));
    node_player->scale(Ogre::Vector3(1,1,1));
    node_player->roll(Ogre::Degree(180));
    node_player->pitch(Ogre::Degree(180));
    node_player->yaw(Ogre::Degree(90));
    node_player->translate(Ogre::Vector3(0,1.5,0));
    ent_player->setMaterialName("MaterialPlayer");
}

  void RankingState::updateMenu(float delta){
    _time += delta;
    _sceneMgr->getSceneNode("Player_rails_ranking_Node")->setPosition(Ogre::Vector3(3.5,Ogre::Math::Sin(_time*10)*0.05-2.6,0));
}
