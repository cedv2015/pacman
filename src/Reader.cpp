#include "Reader.h"

#define ITEM 0
#define WALL 1
#define ENEMY 2
#define PLAYER 3
#define POWERUP 4



using namespace std;

template<> Reader* Ogre::Singleton<Reader>::msSingleton = nullptr;

Reader::Reader() : _graph(new Graph), _player(nullptr), _stage(nullptr), _level(1), _speed(3.0), _gameOver(false) {
    _sceneManager = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
}

Reader::~Reader(){
    if (_graph) {
        delete _graph;
    }
    if (_player) {
        delete _player;
    }
}

Reader* Reader::getSingletonPtr(void){
    return msSingleton;
}

Reader& Reader::getSingleton(void){
    assert(msSingleton);
    return (*msSingleton);
}

void Reader::convertMatrix(string num){
    _gameOver = false;
    string name_screen = "media/stage/Pantalla" + num + ".csv";
    ifstream fmatrix;
    fmatrix.open(name_screen);

    if (fmatrix.fail()) {
        _gameOver = true;
        return;
    }
    destroyStage();
    _stage = _sceneManager->createSceneNode("Stage"+num);
    _sceneManager->getRootSceneNode()->addChild(_stage);
    string line;
    char lineAux[100] ;
    int i=0, j=0;
    getline(fmatrix, line, '\n');
    strcpy( lineAux,line.c_str());
    char *ptr;
    ptr = strtok(lineAux,";");
    _cols = atoi(ptr);
    getline(fmatrix, line, '\n');
    strcpy( lineAux,line.c_str());
    ptr = strtok(lineAux,";");
    _rows = atoi(ptr);
    float aux_cols = _cols;
    float aux_rows = _rows;
    _sceneManager->getCamera("MainCamera")->setPosition(Ogre::Vector3(- (aux_cols / 2) + 0.5, aux_cols, 1.5 * aux_cols - 0.5));
    _sceneManager->getCamera("MainCamera")->lookAt(Ogre::Vector3(- (aux_cols / 2) + 0.5, 0, aux_rows / 2 - 0.5));
    CameraManager::getSingletonPtr()->nextStage(Ogre::Vector3(- (aux_cols / 2) + 0.5, 0, aux_rows / 2 - 0.5));
    _matrix.reserve(_cols * _rows);

    while (!fmatrix.eof()) {

        getline(fmatrix, line, '\n');
        strcpy( lineAux,line.c_str());
        ptr = strtok(lineAux,";");
        while (ptr!=NULL) {
            _matrix[i*_cols+j]=atoi(ptr);
            ptr = strtok(NULL, ";");
            //cout << _matrix[i*_cols+j] << " , ";
            j++;
        }
        //cout <<  endl;
        i++;
        j=0;
    }
    _matrix = transformateMatrix();
    fmatrix.close();
    createGraph();
    read();
    optimizeGraph();
    wallpaper();
}

void Reader::read(){

    Ogre::Entity *ent_aux;
    Ogre::SceneNode *node_aux;

    Ogre::Light* light = _sceneManager->createLight("Light1");
    light->setPosition(0,30,2);
    light->setType(Ogre::Light::LT_SPOTLIGHT);
    light->setDirection(Ogre::Vector3(1,-1,0));
    light->setSpotlightInnerAngle(Ogre::Degree(25.0f));
    light->setSpotlightOuterAngle(Ogre::Degree(190.0f));
    light->setSpotlightFalloff(5.0f);
    light->setCastShadows(true);
    addFloor ();
    int i=0, j=0, aux;
    enemy = 0;
    std::stringstream saux, sauxnode, sauxmesh;
    for (i=0; i<_rows; i++) {
        for (j=0; j<_cols; j++) {
            aux = _matrix[i*_cols+j] ;
            if(aux==WALL){
                wall++;
                saux << "Wall" << wall;
                sauxnode << saux << _level << "_Node";
                ent_aux = _sceneManager->createEntity(saux.str(), "Wall.mesh");
                node_aux = _sceneManager->createSceneNode(sauxnode.str());
		        _stage->addChild(node_aux);
                node_aux->attachObject(ent_aux);
                node_aux->setPosition(Ogre::Vector3(-j,0,i));
                Wall *wall = new Wall(Ogre::Vector2(-j,-i), node_aux, ent_aux);
                _graph->addWall(wall);
            }else if(aux==ITEM){
                point++;
                saux << "Item" << point;
                sauxnode << saux << _level << "_Node";
                ent_aux = _sceneManager->createEntity(saux.str(), "Coin.mesh");
                node_aux = _sceneManager->createSceneNode(sauxnode.str());
		        _stage->addChild(node_aux);
                node_aux->attachObject(ent_aux);
                node_aux->setPosition(Ogre::Vector3(-j,0,i));
                Point *p = new Point(Ogre::Vector2(-j, -i), node_aux, ent_aux);
                _graph->addItem(p);
            }else if(aux==ENEMY){
                enemy++;
                saux << "Enemy" << enemy;
                sauxnode << saux <<  _level << "_Node";
                ent_aux = _sceneManager->createEntity(saux.str(), "Enemy.mesh");
                node_aux = _sceneManager->createSceneNode(sauxnode.str());
		        _stage->addChild(node_aux);
                node_aux->attachObject(ent_aux);
                node_aux->setPosition(Ogre::Vector3(-j,0,i));
                node_aux->scale(Ogre::Vector3(0.35,0.35,0.35));
                Enemy *en= new Enemy(enemy%2, _speed, Ogre::Vector2(-j,-i), node_aux, ent_aux, _graph);
                _graph->addEnemy(en);
                _enemies.push_back(en);
                std::string name_node = std::to_string(j) + ":" + std::to_string(i);
                Node *node_initial = _graph->getNode(name_node);
                en->setInitialNode(node_initial);
                en->attack();
            }else if(aux == POWERUP){
                powerup++;
                saux << "PowerUp" << point;
                sauxnode << saux << _level << "_Node";
                ent_aux = _sceneManager->createEntity(saux.str(), "PowerUp.mesh");
                node_aux = _sceneManager->createSceneNode(sauxnode.str());
		        _stage->addChild(node_aux);
                node_aux->attachObject(ent_aux);
                node_aux->setPosition(Ogre::Vector3(-j,0,i));
                PowerUp *p = new PowerUp(Ogre::Vector2(-j, -i), node_aux, ent_aux);
                _graph->addItem(p);
            }else if(aux==PLAYER){
                player++;
                saux << "Player" << player;
                sauxnode << saux <<  _level << "_Node";
                ent_aux = _sceneManager->createEntity(saux.str(), "Player.mesh");
                node_aux = _sceneManager->createSceneNode(sauxnode.str());
                _stage->addChild(node_aux);
                node_aux->attachObject(ent_aux);
                node_aux->setPosition(Ogre::Vector3(-j,0,i));
                if (_player) {
                    //TODO: añadir nuevo campo a los csv (time limit) para poner el tiempo
                    // en el que el jugador se debería pasar la pantalla
                    _player->nextStage(-j, -i, _graph, node_aux, ent_aux, 100, _cols);
                }
                else {
                    _player = new Player(_speed, Ogre::Vector2(-j,-i), node_aux, ent_aux, _graph, _cols);
                }
                _graph->setPlayer(_player);
                _graph->setPlayerPosition(std::to_string(int(-j)) + ":" + std::to_string(-i));
            }
        }
    }
}

void Reader::createGraph(){
    for(int i=0; i<_rows; i++){
        for(int j=0; j<_cols; j++){
            if(_matrix[i * _cols + j] != WALL){
                std::string name_node = std::to_string(j) + ":" + std::to_string(i);
                std::string name_node_compare;
                _graph->addNode(name_node, j, i);
                if (j != 0){
                    if(_matrix[i * _cols + (j - 1)] != WALL){
                        name_node_compare = std::to_string(j - 1) + ":" + std::to_string(i);
                        _graph->addEdge (name_node,name_node_compare, 1);
                    }
                }
                if (i != 0){
                    if(_matrix[(i - 1) * _cols + j] != WALL){
                        name_node_compare = std::to_string(j) + ":" + std::to_string(i-1);
                        _graph->addEdge (name_node,name_node_compare, 1);
                    }
                }
            }
        }
    }
}

void Reader::optimizeGraph () {
    Node *n = nullptr;
    std::vector<Node*> nodes = _graph->getNodes();
    int limit = _cols - 1;
    for (unsigned int i = 0; i < nodes.size(); i++) {
        n = nodes[i];
        if (n->isEdge()) {
            for (Enemy *e : _graph->getEnemies()) {
                if (e->getNextNode () == n) {
                    e->setInitialNode(n->selectRandomAdyacent());
                }
            }
            _graph->removeNode(i);
            nodes.erase(nodes.begin()+i);
            i--;
        }
        else if (n->getX() % _cols == 0) {
            std::string s = std::to_string(limit) + ":" + std::to_string(-n->getY());
            _graph->addEdge (n->getName(), s, 0);
        }
    }
    Ogre::Vector3 position = _sceneManager->getCamera("MainCamera")->getPosition();
    position.y -= 5;
    position.z -= 5;
    _graph->setTittlePosition(position);
}

Player* Reader::getPlayer(){
    return _player;
}

std::vector<Enemy*> Reader::getEnemies(){
    return _enemies;
}

int Reader::getCols(){
    return _cols;
}

int Reader::getRows(){
    return _rows;
}

int Reader::getLevel(){
    return _level;
}

void Reader::destroyStage(){
    if (_stage) {
        _sceneManager->getRootSceneNode()->removeAllChildren();
        _sceneManager->clearScene();
        std::cout << "Destroying scene... " << std::endl;
    }
    if (_graph) {
        _graph->reset();
    }
    _stage = nullptr;
}

void Reader::loadNextStage () {
    _level++;
    convertMatrix (std::to_string(_level));
}

std::vector<int> Reader::transformateMatrix(){
	std::vector<int> aux;
    aux.reserve(_cols * _rows);
        for(int i=0; i<_rows; i++){
            for(int j=0; j<_cols; j++){
                aux[i * _cols + j] = _matrix[i * _cols + (_cols-1-j)];
            }
        }
    return aux;
}

void Reader::addFloor () {
    Ogre::SceneNode *nod;
    Ogre::Entity *ent;
    int floor = 0;
    for (int i = 0; i < _rows; i++) {
        for (int j = 0; j < _cols; j++) {
            floor = j + i * _cols;
            ent = _sceneManager->createEntity("Floor" + std::to_string(floor), "Floor.mesh");
            nod = _sceneManager->createSceneNode("Floor" + std::to_string(floor));
            nod->setScale(0.5,0.5,0.5);
            nod->attachObject(ent);
            _stage->addChild(nod);
            nod->setPosition(-j, -0.5, i);
        }
    }
}

void Reader::wallpaper(){

    Ogre::TexturePtr m_backgroundTexture = Ogre::TextureManager::getSingleton().createManual("BackgroundTextureCave",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D,640, 480, 0, Ogre::PF_BYTE_BGR,Ogre::TU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
    Ogre::Image m_backgroundImage;
    m_backgroundImage.load("cave.png",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    m_backgroundTexture->loadImage(m_backgroundImage);
    Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("BackgroundMaterialCave", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    material->getTechnique(0)->getPass(0)->createTextureUnitState("BackgroundTextureCave");
    material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    Ogre::Rectangle2D* rect = new Ogre::Rectangle2D(true);
    rect->setCorners(1.0, -1.0, -1.0, 1.0);
    rect->setMaterial("BackgroundMaterialCave");
    rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
    rect->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));
    Ogre::SceneNode* headNode = _sceneManager->getRootSceneNode()->createChildSceneNode("Background"+std::to_string(_level));
    headNode->attachObject(rect);
    material->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(0.0, 0.0); //PARAR
}

void Reader::resetLevel(){
    _level = 1;
}
