#include "ReadyGo.h"

ReadyGo::ReadyGo (Ogre::Vector2 position, Ogre::SceneNode *node, Ogre::Entity *entity) : StaticEntity(position, node, entity), _animation(new AnimationBlender(entity)), _time(0) {
    _animation->blend("RotateLetters", AnimationBlender::Switch, 2, false);
    _node->setVisible(false);
};

bool ReadyGo::update (float delta) {
    if (_animation->mComplete) {
        _node->setVisible(false);
        return true;
    }
    _animation->addTime(delta);
    return false;
}

void ReadyGo::reset (Ogre::SceneNode *node, Ogre::Entity *entity) {
    if (node && entity) {
        _node = node;
        _entity = entity;
        delete _animation;
        _animation = new AnimationBlender(entity);
    }
    _node->setVisible(true);
    _node->resetOrientation();
    _node->yaw(Ogre::Degree(-90));
    _node->roll(Ogre::Degree(45));
    _animation->blend("RotateLetters", AnimationBlender::Switch, 2, false);
}

void ReadyGo::setPosition(Ogre::Vector3 position) {
    _node->setPosition(position);
}
