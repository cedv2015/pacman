#include "Wall.h"

bool Wall::isPlayerColliding (Ogre::Vector2 vec) {
    bool doNotcollidesX = vec.x - _width >= _position.x + _width || vec.x + _width <= _position.x - _width;
    bool doNotcollidesY = vec.y - _width >= _position.y + _width || vec.y + _width <= _position.y - _width;
    if (!doNotcollidesX && !doNotcollidesY) {
        return true;
    }
    return false;
}
