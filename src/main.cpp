#include <iostream>

#include "GameManager.h"
#include "MainState.h"

int main() {
    GameManager* game = new GameManager;

    new MainState;
    new MenuState;
    new PlayState;
    new PauseState;
    new RankingState;
    new InstructionsState;
    new AddRankingState;

    try {
        game->start(MainState::getSingletonPtr());
    } catch (Ogre::Exception& e) {
        std::cout << "Exception" << std::endl;
    }

    delete game;

    return 0;
}
